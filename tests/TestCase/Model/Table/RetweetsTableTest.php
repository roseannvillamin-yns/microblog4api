<?php
declare(strict_types=1);

namespace App\Test\TestCase\Model\Table;

use App\Model\Table\RetweetsTable;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\RetweetsTable Test Case
 */
class RetweetsTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \App\Model\Table\RetweetsTable
     */
    protected $Retweets;

    /**
     * Fixtures
     *
     * @var array
     */
    protected $fixtures = [
        'app.Retweets',
        'app.Posts',
        'app.Users',
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp(): void
    {
        parent::setUp();
        $config = $this->getTableLocator()->exists('Retweets') ? [] : ['className' => RetweetsTable::class];
        $this->Retweets = $this->getTableLocator()->get('Retweets', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown(): void
    {
        unset($this->Retweets);

        parent::tearDown();
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
