<?php

declare(strict_types=1);

namespace App\Controller;

use Cake\I18n\FrozenTime;
use Cake\Mailer\Mailer;
use App\Controller\AppController;
use Cake\Http\Client;
use Cake\Datasource\Exception\RecordNotFoundException;
use Cake\Http\Exception\NotFoundException;

/**
 * Users Controller
 *
 * @property \App\Model\Table\UsersTable $Users
 * @method \App\Model\Entity\User[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class UsersController extends AppController
{
    public function beforeFilter(\Cake\Event\EventInterface $event)
    {
        parent::beforeFilter($event);
        // Configure the login action to not require authentication, preventing
        // the infinite redirect loop issue
        $this->Authentication->addUnauthenticatedActions([
            'login', 'add', 'verification', 'logout', 'forgotPassword', 'changePassword'
        ]);
    }
    /**
     * Index method
     *
     * @return \Cake\Http\Response|null|void Renders view
     */
    public function index()
    {
        //$users = $this->paginate($this->Users);

        //$this->set(compact('users'));
        $this->Authorization->skipAuthorization();
        return $this->redirect([
            'controller' => 'Users',
            'action' => 'login'
        ]);
    }

    /**
     * View method
     *
     * @param string|null $id User id.
     * @return \Cake\Http\Response|null|void Renders view
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($username = null)
    {
        //$user = $this->Authentication->getIdentity();
        $result = $this->Users
                ->findByUsername($username)
                ->first();
        if (empty($result)) {
            throw new NotFoundException(__('User not found.'));
        }
        $userView = $this->Users->get($result['id']);
        $this->Authorization->authorize($userView);
        $url =  "http://" . $_SERVER['HTTP_HOST'] . '/microblog4api/api/viewUser/' . $username . '.json';
        $session = $this->request->getSession();
        $token = $session->read('token');
        $http = new Client([
            'headers' => ['Authorization' => $token],
        ]);

        $response = $http->get($url);
        $post = json_decode($response->getStringBody(), true);
        
        $post['posts'] = $this->Paginator->paginate($this->Posts, $post['page_settings']);

        if ($post['status']['code'] == 200) {
            $posts = $post['posts'];
            $likes = $post['likes'];
            $userView = $post['user'];
            $this->set(compact(['posts', 'likes', 'userView']));
        }
        $this->set('title', "Microblog 2 - User's Page");

        return $this->userInfo();
    }

    /**
     * Add method
     *
    */
    public function add()
    {
        $this->Authorization->skipAuthorization();
        $user = $this->Users->newEmptyEntity();
        if ($this->request->is('post')) {
            $user = $this->Users->patchEntity($user, $this->request->getData());
            $url =  "http://" . $_SERVER['HTTP_HOST'] . '/microblog4api/api/add.json';
            $http = new Client();
            $response = $http->post($url, [
                'full_name' => $this->request->getData('full_name'),
                'username' => $this->request->getData('username'),
                'email' => $this->request->getData('email'),
                'password' => $this->request->getData('password')
            ]);
            $userData = json_decode($response->getStringBody(), true);
            if ($userData['status']['code'] == 201) {
                $this->Flash->success(__('The user has been saved. Kindly check your email.'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The user could not be saved. Please, try again.'));
            }
        }
        $this->set(compact('user'));

        $this->set('title', 'Microblog 4 - Sign-up');
        $this->viewBuilder()->setLayout('navbar_signup');
    }

    public function verification($username, $code)
    {
        $this->Authorization->skipAuthorization();
        $url =  "http://" . $_SERVER['HTTP_HOST'] . '/microblog4api/api/verification/'
        . $username . '/' . $code . '.json';
        $http = new Client();
        $response = $http->patch($url);
        $user = json_decode($response->getStringBody(), true);
        if ($user['status']['code'] == 200) {
            $this->Flash->success(__('Registration Completed.'));
            $this->redirect(['action' => 'index']);
        } else {
            $this->Flash->error(__('Activation Failed / Account already verified.'));
            $this->redirect(['action' => 'index']);
        }
    }

    public function login()
    {
        $this->Authorization->skipAuthorization();
        $session = $this->request->getSession();
        if ($session->read('Auth.verified') == 1) {
            $this->Flash->error(__('You are logged in!'));
            $this->redirect(['controller' => 'Posts', 'action' => 'index']);
        }
        $this->request->allowMethod(['get', 'post']);
        $result = $this->Authentication->getResult();
        if ($this->request->getData('username') != null) {
            $url =  "http://" . $_SERVER['HTTP_HOST'] . '/microblog4api/api/login.json';
            $http = new Client();
            $response = $http->post($url, [
                'username' => $this->request->getData('username'),
                'password' => $this->request->getData('password')
            ]);
            $user = json_decode($response->getStringBody(), true);
            if ($result->isValid()) {
                if ($user['status']['code'] == 200) {
                    $session->write('Auth', $user['user']);
                    $session->write('token', $user['user']['access_token']);
                    $this->Flash->success(__('Successfully logged in.'));
                    $redirect = $this->request->getQuery('redirect', [
                        'controller' => 'Posts',
                        'action' => 'index',
                    ]);

                    return $this->redirect($redirect);
                } else {
                    $session->destroy('Auth');
                    $this->Flash->error(__(
                        'Invalid username/password, or your account is not yet verified. Try again.'
                    ));
                }
            }
        }
        // display error if user submitted and authentication failed
        if ($this->request->is('post') && !$result->isValid()) {
            $this->Flash->error(__('Invalid email or password'));
        }
        $user = $this->Users->newEmptyEntity();
        $this->set(compact('user'));

        $this->set('title', 'Microblog 4 - Login');
        $this->viewBuilder()->setLayout('navbar_login');
    }

    public function logout()
    {
        $this->Authorization->skipAuthorization();
        $result = $this->Authentication->getResult();
        if ($result->isValid()) {
            $this->Flash->success(__('You have successfully logged out!'));
            $this->Authentication->logout();
            $session = $this->request->getSession();
            $session->destroy('Auth');
            return $this->redirect(['controller' => 'Users', 'action' => 'login']);
        }
    }

    public function edit($id = null)
    {
        $userAuth = $this->Authentication->getIdentity();
        $user = $this->Users->get($id);
        if (empty($id)) {
            throw new NotFoundException();
        } elseif ($user['id'] != $userAuth['id']) {
            $this->Flash->error(__('You are not authorized to this page.'));
        }
        $this->Authorization->authorize($user);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $user = $this->Users->patchEntity($user, $this->request->getData());
            $url =  "http://" . $_SERVER['HTTP_HOST'] . '/microblog4api/api/editUser/' . $id . '.json';
            $session = $this->request->getSession();
            $token = $session->read('token');
            $http = new Client([
                'headers' => ['Authorization' => $token],
            ]);
            if ($this->request->getData('profile_image')->getClientFilename() != '') {
                $data = [
                    'full_name' => $this->request->getData('full_name'),
                    'username' => $this->request->getData('username'),
                    'email' => $this->request->getData('email'),
                    'new_password' => $this->request->getData('new_password'),
                    'profile_image' => $this->request->getData('profile_image')->getClientFilename(),
                    'profile_pic' => $this->request->getData('profile_image')->getClientFilename()
                ];
            } else {
                $data = [
                    'full_name' => $this->request->getData('full_name'),
                    'username' => $this->request->getData('username'),
                    'email' => $this->request->getData('email'),
                    'new_password' => $this->request->getData('new_password'),
                ];
            }

            $response = $http->post($url, $data);
            $editUser = json_decode($response->getStringBody(), true);

            if ($editUser['status']['code'] == 200) {
                if (!empty($data['profile_pic'])) {
                    $targetPath = WWW_ROOT . 'img' . DS . 'profiles' . DS . $data['profile_pic'];
                    $this->request->getData('profile_image')->moveTo($targetPath);
                }
                $userLoggedIn = $this->Authentication->getIdentity();
                $session = $this->request->getSession();
                $session->write('Auth', array_merge(
                    $this->Users->get($userLoggedIn['id'])->toArray(),
                    $this->request->getData()
                ));
                $this->Flash->success(__('The user has been updated.'));
                return $this->redirect(['controller' => 'Posts', 'action' => 'index']);
            } else {
                $this->Flash->error(__('The post could not be updated. Please, try again.'));
            }
        }
        $this->set(compact('user'));

        $this->set('title', 'Microblog 4 - Account Settings');

        return $this->userInfo();
    }

    public function viewUsers()
    {
        $user = $this->Authentication->getIdentity();
        $userAuthorized = $this->Users->get($user['id']);
        $this->Authorization->authorize($userAuthorized);

        $url =  "http://" . $_SERVER['HTTP_HOST'] . '/microblog4api/api/viewUsers.json';
        $session = $this->request->getSession();
        $token = $session->read('token');
        $http = new Client([
            'headers' => ['Authorization' => $token],
        ]);

        $response = $http->get($url);
        $viewUsers = json_decode($response->getStringBody(), true);
        $viewUsers['users'] = $this->Paginator->paginate($this->Users, $viewUsers['page_settings']);

        if ($viewUsers['status']['code'] == 200) {
            $users = $viewUsers['users'];
            $this->set(compact(['users']));
        }

        $this->set('title', 'Microblog 4 - All Users');

        return $this->userInfo();
    }

    public function forgotPassword()
    {
        $this->Authorization->skipAuthorization();
        if ($this->request->is(['post', 'put'])) {
            $url =  "http://" . $_SERVER['HTTP_HOST'] . '/microblog4api/api/forgotPassword.json';
            $http = new Client();
            $response = $http->put($url, [
                'email' => $this->request->getData('email')
            ]);
            $user = json_decode($response->getStringBody(), true);
            if ($user['status']['code'] == 200) {
                $this->Flash->success(__('Email Sent'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('Invalid email or your account is not yet verified. Try again.'));
            }
        }

        $this->set('title', 'Microblog 4 - Forgot Password');
        $this->viewBuilder()->setLayout('navbar_signup');
    }

    public function changePassword($code = null)
    {
        $this->Authorization->skipAuthorization();
        $user = $this->Users->newEmptyEntity();
        if (!$code) {
            throw new NotFoundException(__('Invalid code'));
        }

        $result = $this->Users
            ->findByEmailToken($code)
            ->first();
        if ($result['verified'] == 0 && $result['email_token'] === $code) {
            if ($this->request->is(['patch', 'post', 'put'])) {
                $user = $this->Users->patchEntity($result, $this->request->getData());
                $url =  "http://" . $_SERVER['HTTP_HOST'] . '/microblog4api/api/changePassword/' . $code . '.json';
                $http = new Client();
                $response = $http->patch($url, [
                    'new_password' => $this->request->getData('new_password')
                ]);
                $userData = json_decode($response->getStringBody(), true);
                if ($userData['status']['code'] == 200) {
                    $this->Flash->success(__('Successfully changed your password.'));
                    return $this->redirect(['action' => 'index']);
                } else {
                    $this->Flash->error(__('Failed to change your password.'));
                }
            }
        } else {
            $this->Flash->error(__('Email token already used.'));
            $this->redirect(['action' => 'index']);
        }
        $this->set(compact('user'));

        $this->set('title', 'Microblog 4 - Change Password');
        $this->viewBuilder()->setLayout('navbar_signup');
    }
}
