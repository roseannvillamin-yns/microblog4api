<?php

declare(strict_types=1);

namespace App\Controller\Api;

use App\Controller\AppController;
use Cake\Http\Exception\ConflictException;
use Cake\Http\Exception\NotFoundException;
use Cake\Http\Exception\UnauthorizedException;
use Cake\I18n\FrozenTime;
use Cake\Mailer\Mailer;
use Cake\Utility\Security;
use Firebase\JWT\JWT;

/**
 * Users Controller
 *
 * @property \App\Model\Table\UsersTable $Users
 * @method \App\Model\Entity\User[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class FollowersController extends AppController
{
	public function initialize(): void
    {
        parent::initialize();

        $this->loadComponent('Paginator');
        $this->loadComponent('RequestHandler');
        $this->loadComponent('MyAuth');

        $this->RequestHandler->renderAs($this, 'json');
    }

    public function follow($id)
    {
        $status = array();
        $this->request->allowMethod(['post']);
        $headerAuth = $this->request->getHeaderLine('Authorization');
        $header = $this->JWTTokens->checkHeaderAuth($headerAuth);
        $result = $this->Users->findById($id)
            ->first();
        if (empty($result)) {
            throw new NotFoundException();
        }
        $following = $this->Users->get($id);
        $this->Authorization->authorize($following);
        $user = $this->Authentication->getIdentity();
        if (isset($header)) {
            $userInfo = $this->JWTTokens->checkToken($header);
        }
        if (!empty($userInfo)) {
            if ((!empty($following)) && ($following['verified'] == 1)) {
                $conditions = [
                    'Followers.user_id' => $following['id'],
                    'Followers.follower_user_id' => $user['id']
                ];
                $followUser = $this->Followers->find('all', [
                    'conditions' => $conditions
                ]);
                $follow = $followUser->first();

                if (!empty($follow)) {
                    if ($follow['deleted'] == 1) {
                        $follow['deleted'] = 0;
                        $follow['modified'] = FrozenTime::now('Asia/Hong_Kong');
                        $this->Followers->save($follow);
                        $code = 200;
                        $status = $this->Response->responseFormat($code);
                    } else {
                        $code = 403;
                        $status = $this->Response->responseFormat($code);
                    }
                    $this->response = $this->response->withStatus($status['code']);
                    $this->set(compact(['status', 'follow']));
                    $this->viewBuilder()->setOption('serialize', ['status', 'follow']);
                } else {
                    $new_following = $this->Followers->newEmptyEntity();
                    $new_following['user_id'] = $following['id'];
                    $new_following['follower_user_id'] = $user['id'];
                    if ($this->Followers->save($new_following)) {
                        $code = 201;
                        $status = $this->Response->responseFormat($code);
                    } else {
                        $code = 403;
                        $status = $this->Response->responseFormat($code);
                    }
                    $this->response = $this->response->withStatus($status['code']);
                    $this->set(compact(['status', 'new_following']));
                    $this->viewBuilder()->setOption('serialize', ['status', 'new_following']);
                }
            } else {
                $code = 404;
                $status = $this->Response->responseFormat($code);
                $this->response = $this->response->withStatus($status['code']);
                $this->set(compact(['status']));
                $this->viewBuilder()->setOption('serialize', ['status']);
            }
        } else {
            $code = 401;
            $status = $this->Response->responseFormat($code);
            $status['error'] = 'Authorization token is not valid.';
            $this->response = $this->response->withStatus($code);
            $this->set(compact(['status']));
            $this->viewBuilder()->setOption('serialize', ['status']);
        }
    }

    public function unfollow($id)
    {
        $status = array();
        $this->request->allowMethod(['delete']);
        $headerAuth = $this->request->getHeaderLine('Authorization');
        $header = $this->JWTTokens->checkHeaderAuth($headerAuth);
        $result = $this->Users->findById($id)
            ->first();
        if (empty($result)) {
            throw new NotFoundException();
        }
        $following = $this->Users->get($id);
        $this->Authorization->authorize($following);
        $user = $this->Authentication->getIdentity();
        if (isset($header)) {
            $userInfo = $this->JWTTokens->checkToken($header);
        }
        if (!empty($userInfo)) {
            if ($following != null) {
                $conditions = [
                    'Followers.user_id' => $following['id'],
                    'Followers.follower_user_id' => $user['id']
                ];
                $unfollowUser = $this->Followers->find('all', [
                    'conditions' => $conditions
                ]);
                $unfollow = $unfollowUser->first();

                if (!empty($unfollow)) {
                    if ($unfollow['deleted'] == 0) {
                        $unfollow['deleted'] = 1;
                        $unfollow['modified'] = FrozenTime::now('Asia/Hong_Kong');
                        $this->Followers->save($unfollow);
                        $code = 200;
                        $status = $this->Response->responseFormat($code);
                    } else {
                        $code = 403;
                        $status = $this->Response->responseFormat($code);
                    }
                    $this->response = $this->response->withStatus($status['code']);
                    $this->set(compact(['status', 'unfollow']));
                    $this->viewBuilder()->setOption('serialize', ['status', 'unfollow']);
                } else {
                    $code = 404;
                    $status = $this->Response->responseFormat($code);
                    $this->response = $this->response->withStatus($status['code']);
                    $this->set(compact(['status']));
                    $this->viewBuilder()->setOption('serialize', ['status']);
                }
            }
        } else {
            $code = 401;
            $status = $this->Response->responseFormat($code);
            $status['error'] = 'Authorization token is not valid.';
            $this->response = $this->response->withStatus($code);
            $this->set(compact(['status']));
            $this->viewBuilder()->setOption('serialize', ['status']);
        }
    }

    public function viewFollowers()
    {
        $status = array();
        $this->request->allowMethod(['get']);
        $user = $this->Authentication->getIdentity();
        $userAuthorized = $this->Users->get($user['id']);
        $this->Authorization->authorize($userAuthorized);
        $headerAuth = $this->request->getHeaderLine('Authorization');
        $header = $this->JWTTokens->checkHeaderAuth($headerAuth);
        if (isset($header)) {
            $userInfo = $this->JWTTokens->checkToken($header);
        }
        if (!empty($userInfo)) {
            $following = [
                'Followers.user_id' => $user['id'],
                'Followers.deleted' => 0
            ];
            $page_settings = [
                'conditions' => $following,
                'contain' => ['UsersFollowing', 'UsersFollowers'],
            ];
            $user_followers = $this->Paginator->paginate($this->Followers, $page_settings);
            $page = $this->request->getAttribute('paging');

            $code = 200;
            $status = $this->Response->responseFormat($code);
            $this->response = $this->response->withStatus($status['code']);
            $this->set(compact(['status', 'user_followers', 'page_settings']));
            $this->viewBuilder()->setOption('serialize', ['status', 'user_followers', 'page_settings']);
        } else {
            $code = 401;
            $status = $this->Response->responseFormat($code);
            $status['error'] = 'Authorization token is not valid.';
            $this->response = $this->response->withStatus($code);
            $this->set(compact(['status']));
            $this->viewBuilder()->setOption('serialize', ['status']);
        }
    }

    public function viewFollowing()
    {
        $status = array();
        $this->request->allowMethod(['get']);
        $user = $this->Authentication->getIdentity();
        $userAuthorized = $this->Users->get($user['id']);
        $this->Authorization->authorize($userAuthorized);
        $headerAuth = $this->request->getHeaderLine('Authorization');
        $header = $this->JWTTokens->checkHeaderAuth($headerAuth);
        if (isset($header)) {
            $userInfo = $this->JWTTokens->checkToken($header);
        }
        if (!empty($userInfo)) {
            $following = [
                'Followers.follower_user_id' => $user['id'],
                'Followers.deleted' => 0
            ];
            $page_settings = [
                'conditions' => $following,
                'contain' => ['UsersFollowing', 'UsersFollowers'],
                'limit' => 5,
                'order' => [
                    'Followers.modified' => 'desc'
                ]
            ];
            $user_following = $this->Paginator->paginate($this->Followers, $page_settings);
            $page = $this->request->getAttribute('paging');

            $code = 200;
            $status = $this->Response->responseFormat($code);
            $this->response = $this->response->withStatus($status['code']);
            $this->set(compact(['status', 'user_following', 'page_settings']));
            $this->viewBuilder()->setOption('serialize', ['status', 'user_following', 'page_settings']);
        } else {
            $code = 401;
            $status = $this->Response->responseFormat($code);
            $status['error'] = 'Authorization token is not valid.';
            $this->response = $this->response->withStatus($code);
            $this->set(compact(['status']));
            $this->viewBuilder()->setOption('serialize', ['status']);
        }
    }
}