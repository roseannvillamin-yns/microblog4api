<?php

declare(strict_types=1);

namespace App\Controller\Api;

use App\Controller\AppController;
use Cake\Http\Exception\ConflictException;
use Cake\Http\Exception\NotFoundException;
use Cake\Http\Exception\UnauthorizedException;
use Cake\I18n\FrozenTime;
use Cake\Mailer\Mailer;
use Cake\Utility\Security;
use Firebase\JWT\JWT;

/**
 * Users Controller
 *
 * @property \App\Model\Table\UsersTable $Users
 * @method \App\Model\Entity\User[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class CommentsController extends AppController
{
	public function initialize(): void
    {
        parent::initialize();

        $this->loadComponent('Paginator');
        $this->loadComponent('RequestHandler');
        $this->loadComponent('MyAuth');

        $this->RequestHandler->renderAs($this, 'json');
    }

    public function add($id = null)
    {
        $status = array();
        $this->request->allowMethod(['post', 'get']);
        $comment = $this->Comments->newEmptyEntity();
        $this->Authorization->authorize($comment);
        $headerAuth = $this->request->getHeaderLine('Authorization');
        $header = $this->JWTTokens->checkHeaderAuth($headerAuth);
        $userLogged = $this->Users->findByAccessToken($header)->first();
        $result = $this->Posts->findById($id)
            ->first();
        if (empty($result)) {
            throw new NotFoundException(__('Post not found.'));
        }
        if (isset($header)) {
            $userInfo = $this->JWTTokens->checkToken($header);
        }
        if (!empty($userInfo)) {
            $user = $this->Authentication->getIdentity();
            $post = $this->Posts->get($id, [
                'contain' => ['Users']
            ]);
            $conditions = [
                'Comments.deleted' => 0,
                'Comments.post_id' => $post['id']
            ];
            $this->set(compact('post', [
                'conditions' => $conditions
            ]));
            $commentQuery = $this->Comments->find(
                'all',
                array(
                    'conditions' => $conditions,
                    'contain' => ['Users', 'Posts']
                )
            );
            $comment_count = $commentQuery->count();

            $page_settings = [
                'conditions' => $conditions,
                'contain' => ['Users', 'Posts'],
                'limit' => 5,
                'order' => [
                    'created' => 'desc'
                ]
            ];
            $post_comments = $this->Paginator->paginate($this->Comments, $page_settings);
            $page = $this->request->getAttribute('paging');

            if ($this->request->is('post')) {
                $comment = $this->Comments->patchEntity($comment, $this->request->getData());
                $comment['post_id'] = $post['id'];
                $comment['user_id'] = $user['id'];
                if (!$comment->getErrors()) {
                    if ($this->Comments->save($comment)) {
                        $code = 201;
                        $status = $this->Response->responseFormat($code);
                    }
                } else {
                    $code = 400;
                    $status = $this->Response->responseFormat($code);
                    $status['error'] = $comment->getErrors();
                }
                $this->response = $this->response->withStatus($status['code']);
                $this->set(compact([
                    'status', 'post', 'comment_count', 'post_comments', 'page_settings'
                ]));
                $this->viewBuilder()->setOption('serialize', [
                    'status', 'post', 'comment_count', 'post_comments', 'page_settings'
                ]);
            } else {
                $this->set(compact(['post', 'comment_count', 'post_comments', 'page_settings']));
                $this->viewBuilder()->setOption('serialize', [
                    'post', 'comment_count', 'post_comments', 'page_settings'
                ]);
            }
        } else {
            $code = 401;
            $status = $this->Response->responseFormat($code);
            $status['error'] = 'Authorization token is not valid.';
            $this->response = $this->response->withStatus($code);
            $this->set(compact(['status']));
            $this->viewBuilder()->setOption('serialize', ['status']);
        }
    }

    public function edit($id = null)
    {
        $this->request->allowMethod(['put']);
        $headerAuth = $this->request->getHeaderLine('Authorization');
        $header = $this->JWTTokens->checkHeaderAuth($headerAuth);
        $user = $this->Users->findByAccessToken($header)->first();
        $result = $this->Comments->findById($id)
            ->first();
        if (empty($result)) {
            throw new NotFoundException(__('Comment not found.'));
        } elseif ($user['id'] != $result['user_id']) {
            throw new UnauthorizedException(__('Unauthorized.'));
        }
        $comment = $this->Comments->get($id);
        $this->Authorization->authorize($comment);
        if (isset($header)) {
            $userInfo = $this->JWTTokens->checkToken($header);
        }
        if (!empty($userInfo)) {
            if ($this->request->is(['put'])) {
                $comment = $this->Comments->patchEntity($comment, $this->request->getData());
                if (!$comment->getErrors()) {
                    if ($this->Comments->save($comment)) {
                        $code = 200;
                        $status = $this->Response->responseFormat($code);
                    }
                } else {
                    $code = 401;
                    $status = $this->Response->responseFormat($code);
                    $status['error'] = $comment->getErrors();
                }
                $this->response = $this->response->withStatus($status['code']);
                $this->set(compact(['status', 'comment']));
                $this->viewBuilder()->setOption('serialize', ['status', 'comment']);
            }
        } else {
            $code = 401;
            $status = $this->Response->responseFormat($code);
            $status['error'] = 'Authorization token is not valid.';
            $this->response = $this->response->withStatus($code);
            $this->set(compact(['status']));
            $this->viewBuilder()->setOption('serialize', ['status']);
        }
    }

    // Comment Delete
    public function delete($id = null)
    {
        $this->request->allowMethod(['delete']);
        $headerAuth = $this->request->getHeaderLine('Authorization');
        $header = $this->JWTTokens->checkHeaderAuth($headerAuth);
        $user = $this->Users->findByAccessToken($header)->first();
        $result = $this->Comments->findById($id)
            ->first();
        if (empty($result)) {
            throw new NotFoundException(__('Record not found.'));
        } elseif ($user['id'] != $result['user_id']) {
            throw new UnauthorizedException();
        }
        $comment = $this->Comments->get($id);
        $this->Authorization->authorize($comment);
        if (isset($header)) {
            $userInfo = $this->JWTTokens->checkToken($header);
        }
        if (!empty($userInfo)) {
            $comment['deleted'] = 1;
            if ($this->Comments->save($comment)) {
                $code = 200;
                $status = $this->Response->responseFormat($code);
            } else {
                $code = 401;
                $status = $this->Response->responseFormat($code);
            }
            $this->response = $this->response->withStatus($status['code']);
            $this->set(compact(['status', 'comment']));
            $this->viewBuilder()->setOption('serialize', ['status', 'comment']);
        } else {
            $code = 401;
            $status = $this->Response->responseFormat($code);
            $status['error'] = 'Authorization token is not valid.';
            $this->response = $this->response->withStatus($code);
            $this->set(compact(['status']));
            $this->viewBuilder()->setOption('serialize', ['status']);
        }
    }
}