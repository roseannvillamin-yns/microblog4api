<?php

declare(strict_types=1);

namespace App\Controller\Api;

use App\Controller\AppController;
use Cake\Http\Exception\ConflictException;
use Cake\Http\Exception\NotFoundException;
use Cake\Http\Exception\UnauthorizedException;
use Cake\I18n\FrozenTime;
use Cake\Mailer\Mailer;
use Cake\Utility\Security;
use Firebase\JWT\JWT;

/**
 * Users Controller
 *
 * @property \App\Model\Table\UsersTable $Users
 * @method \App\Model\Entity\User[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class UsersController extends AppController
{
	public function initialize(): void
    {
        parent::initialize();

        $this->loadComponent('Paginator');
        $this->loadComponent('RequestHandler');
        $this->loadComponent('MyAuth');

        $this->RequestHandler->renderAs($this, 'json');
    }

    public function beforeFilter(\Cake\Event\EventInterface $event)
    {
        parent::beforeFilter($event);
        // Configure the login action to not require authentication, preventing
        // the infinite redirect loop issue
        $this->Authentication->addUnauthenticatedActions([
            'login', 'add', 'verification', 'logout', 'forgotPassword', 'changePassword', 'unauthenticated'
        ]);
    }

	public function login()
    {
        $status = array();
        $this->Authorization->skipAuthorization();
        $this->request->allowMethod(['post']);
        if ($code = 500) {
            $code = 500;
            $status = $this->Response->responseFormat($code);
            $this->response = $this->response->withStatus($status['code']);
            $this->set(compact(['status']));
            $this->viewBuilder()->setOption('serialize', ['status']);
        }
        $password = $this->request->getData('password');

        if ($this->request->getData('username') != null) {
            $user = $this->Users
                ->findByUsername($this->request->getData('username'))
                ->first();
            $userPass = $user['password'];
            if ($user != null) {
                $result = $this->MyAuth->checkUser($password, $userPass);
                if ($result == true && $user['verified'] == 1) {
                    $id = $user['id'];
                    $json = $this->JWTTokens->generateAccessToken($id);
                    $access_token = $json['token'];
                    $user['access_token'] = $access_token;
                    if ($this->Users->save($user)) {
                        $code = 200;
                        $status = $this->Response->responseFormat($code);
                        $json = $user['access_token'];
                    }
                    $this->response = $this->response->withStatus($status['code']);
                    $this->set(compact(['status', 'user']));
                    $this->viewBuilder()->setOption('serialize', ['status', 'user']);
                } else {
                    $code = 401;
                    $status = $this->Response->responseFormat($code);
                    $status['error'] = 'Invalid username or password.';
                    $this->response = $this->response->withStatus($status['code']);
                    $this->set(compact(['status']));
                    $this->viewBuilder()->setOption('serialize', ['status']);
                }
            } else {
                $code = 401;
                $status = $this->Response->responseFormat($code);
                $status['error'] = 'Invalid username or password.';
                $this->response = $this->response->withStatus($status['code']);
                $this->set(compact(['status']));
                $this->viewBuilder()->setOption('serialize', ['status']);
            }
        }
    }

    // User's Add
    public function add()
    {
        $this->Authorization->skipAuthorization();
        $user = $this->Users->newEmptyEntity();
        $this->request->allowMethod(['post']);
        if ($this->request->is('post')) {
            $code = md5($this->request->getData('username'));
            $user['email_token'] = $code;
            $user['created'] = FrozenTime::now('Asia/Hong_Kong');
            $recipient = $this->request->getData('email');
            $username = $this->request->getData('username');
            $user = $this->Users->patchEntity($user, $this->request->getData());
            if (!$user->getErrors()) {
                if ($this->Users->save($user)) {
                    $body = 'To complete the registration, kindly click the link for activation: ';
                    $link = 'http://localhost/microblog4api/users/verification/' . $username . '/' . $code;
                    $message = $body . $link;
                    $mailer = new Mailer();
                    $mailer->setFrom(['cakemicroblog@gmail.com' => 'Microblog CakePHP 4'])
                        ->setTo($recipient)
                        ->setSubject('Account Activation')
                        ->deliver($message);

                    $code = 201;
                    $status = $this->Response->responseFormat($code);
                }
            } else {
                $code = 400;
                $status = $this->Response->responseFormat($code);
                $status['error'] = $user->getErrors();
            }
            $this->response = $this->response->withStatus($status['code']);
            $this->set(compact(['status']));
            $this->viewBuilder()->setOption('serialize', ['status']);
        }
    }

    // User's Verification
    public function verification($username, $code)
    {
        $this->Authorization->skipAuthorization();
        $this->request->allowMethod(['patch']);
        $user = $this->Users
                ->findByUsername($username)
                ->first();
        if ($user['verified'] == 0 && $user['email_token'] === $code) {
            $user['verified'] = 1;
            $this->Users->save($user);
            $code = 200;
            $status = $this->Response->responseFormat($code);
        } else {
            $code = 404;
            $status = $this->Response->responseFormat($code);
        }
        $this->response = $this->response->withStatus($status['code']);
        $this->set(compact(['status']));
        $this->viewBuilder()->setOption('serialize', ['status']);
    }

    // User's View
    public function view($username = null)
    {
        $this->RequestHandler->renderAs($this, 'json');
        $status = array();
        $this->request->allowMethod(['get']);
        $userData = $this->Authentication->getIdentity();
        $result = $this->Users
                ->findByUsername($username)
                ->first();
        if (empty($result)) {
            throw new NotFoundException(__('User not found.'));
        }

        $user = $this->Users->get($result['id']);
        $this->Authorization->authorize($user);
        $headerAuth = $this->request->getHeaderLine('Authorization');
        $header = $this->JWTTokens->checkHeaderAuth($headerAuth);
        if (isset($header)) {
            $userInfo = $this->JWTTokens->checkToken($header);
        }

        if (!empty($userInfo)) {
            $conditions = [
                'Posts.deleted' => 0,
                'Posts.user_id' => $user['id']
            ];

            $page_settings = [
                'conditions' => $conditions,
                'contain' => 'Users',
                'limit' => 10,
                'order' => [
                    'Posts.created' => 'desc'
                ]
            ];

            $posts = $this->Paginator->paginate($this->Posts, $page_settings);
            $page = $this->request->getAttribute('paging');

            $likeConditions = [
                'PostLikes.user_id' => $userData['id'],
                'PostLikes.deleted' => 0
            ];
            $likes = $this->PostLikes->find(
                'all',
                [
                    'conditions' => $likeConditions,
                    'contain' => 'Users', 'Posts',
                    'order' => ['PostLikes.created' => 'desc'],
                ]
            );

            $this->set('likes', $likes);

            if ($result != null) {
                $code = 200;
                $status = $this->Response->responseFormat($code);
            }

            $this->response = $this->response->withStatus($status['code']);
            $this->set(compact([
                'status', 'user', 'posts', 'likes', 'page_settings'
            ]));
            $this->viewBuilder()->setOption('serialize', [
                'status',  'user', 'posts', 'likes', 'page_settings'
            ]);
        } else {
            $code = 401;
            $status = $this->Response->responseFormat($code);
            $status['error'] = 'Authorization token is not valid.';
            $this->response = $this->response->withStatus($code);
            $this->set(compact(['status']));
            $this->viewBuilder()->setOption('serialize', ['status']);
        }
    }

    //User's Edit
    public function edit($id = null)
    {
        $status = array();
        $this->request->allowMethod(['put']);
        $headerAuth = $this->request->getHeaderLine('Authorization');
        $header = $this->JWTTokens->checkHeaderAuth($headerAuth);
        $userLogged = $this->Users->findByAccessToken($header)->first();
        $result = $this->Users->findById($id)
            ->first();
        if (empty($result)) {
            throw new NotFoundException();
        } elseif ($userLogged['id'] != $result['id']) {
            throw new UnauthorizedException();
        }
        $user = $this->Users->get($id);
        $this->Authorization->authorize($user);
        if (isset($header)) {
            $userInfo = $this->JWTTokens->checkToken($header);
        }
        if (!empty($userInfo)) {
            if ($this->request->is(['put'])) {
                $user = $this->Users->patchEntity($user, $this->request->getData());
                $image = $this->request->getData('profile_image')->getClientFilename();
                $user['profile_pic'] = $image;
                $user['password'] = $this->request->getData('new_password');
                if (!$user->getErrors()) {
                    if ($this->Users->save($user)) {
                        if (!empty($image)) {
                            $targetPath = WWW_ROOT . 'img' . DS . 'profiles' . DS . $image;
                            $this->request->getData('profile_image')->moveTo($targetPath);
                        }
                        $code = 200;
                        $status = $this->Response->responseFormat($code);
                    }
                } else {
                    $code = 400;
                    $status = $this->Response->responseFormat($code);
                    $status['error'] = $user->getErrors();
                }
                $this->response = $this->response->withStatus($status['code']);
                $this->set(compact(['status', 'user']));
                $this->viewBuilder()->setOption('serialize', ['status', 'user']);
            }
        } else {
            $code = 401;
            $status = $this->Response->responseFormat($code);
            $status['error'] = 'Authorization token is not valid.';
            $this->response = $this->response->withStatus($code);
            $this->set(compact(['status']));
            $this->viewBuilder()->setOption('serialize', ['status']);
        }
    }

    // Users All Users
    public function viewUsers()
    {
        $status = array();
        $this->request->allowMethod(['get']);
        $headerAuth = $this->request->getHeaderLine('Authorization');
        $header = $this->JWTTokens->checkHeaderAuth($headerAuth);
        $user = $this->Authentication->getIdentity();
        $userAuthorized = $this->Users->get($user['id']);
        $this->Authorization->authorize($userAuthorized);
        if (isset($header)) {
            $userInfo = $this->JWTTokens->checkToken($header);
        }
        if (!empty($userInfo)) {
            $userConditions = [
                'Users.verified' => 1
            ];
            $page_settings = [
                'conditions' => $userConditions,
                'limit' => 10,
                'order' => [
                    'created' => 'desc'
                ]
            ];
            $users = $this->Paginator->paginate($this->Users, $page_settings);
            $page = $this->request->getAttribute('paging');

            $code = 200;
            $status = $this->Response->responseFormat($code);
            $this->response = $this->response->withStatus($status['code']);
            $this->set(compact(['status', 'users', 'page_settings']));
            $this->viewBuilder()->setOption('serialize', ['status', 'users', 'page_settings']);
        } else {
            $code = 401;
            $status = $this->Response->responseFormat($code);
            $status['error'] = 'Authorization token is not valid.';
            $this->response = $this->response->withStatus($code);
            $this->set(compact(['status']));
            $this->viewBuilder()->setOption('serialize', ['status']);
        }
    }

    // User's Forgot Password
    public function forgotPassword()
    {
        $status = array();
        $this->request->allowMethod(['put']);
        $this->Authorization->skipAuthorization();
        if ($this->request->is(['put'])) {
            $user = $this->Users
                ->findByEmail($this->request->getData('email'))
                ->first();
            if (!empty($user) && $user['verified'] == 1) {
                $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
                $code = substr(str_shuffle($characters), 0, 32);
                $user['verified'] = 0;
                $user['email_token'] = $code;
                $recipient = $user['email'];
                $username = $user['username'];
                $user['modified'] = FrozenTime::now('Asia/Hong_Kong');

                if ($this->Users->save($user)) {
                    $body = 'Kindly click the link to change password: ';
                    $link = 'http://localhost/microblog4api/users/changePassword/' . $code;
                    $message = $body . $link;
                    $mailer = new Mailer();
                    $mailer->setFrom(['cakemicroblog@gmail.com' => 'Microblog CakePHP 4'])
                        ->setTo($recipient)
                        ->setSubject('Change Password')
                        ->deliver($message);
                        $this->Flash->success(__('Email Sent'));

                    $code = 200;
                    $status = $this->Response->responseFormat($code);
                }
            } else {
                $code = 404;
                $status = $this->Response->responseFormat($code);
            }
            $this->response = $this->response->withStatus($status['code']);
            $this->set(compact(['status']));
            $this->viewBuilder()->setOption('serialize', ['status']);
        }
    }

    //User's Change Password
    public function changePassword($code = null)
    {
        $status = array();
        $this->request->allowMethod(['patch']);
        $this->Authorization->skipAuthorization();

        $result = $this->Users
            ->findByEmailToken($code)
            ->first();
        if (empty($result)) {
            throw new NotFoundException();
        }
        if ($this->request->is(['patch', 'post', 'put'])) {
            if ($result['verified'] == 0 && $result['email_token'] === $code) {
                $user = $this->Users->patchEntity($result, $this->request->getData());
                $user['verified'] = 1;
                $user['password'] = $this->request->getData('new_password');
                $user['modified'] = FrozenTime::now('Asia/Hong_Kong');
                $user['full_name'] = $user['full_name'];
                $user['username'] = $user['username'];
                $user['email'] = $user['email'];
                if (!$user->getError('new_password')) {
                    if ($this->Users->save($user)) {
                        $code = 200;
                        $status = $this->Response->responseFormat($code);
                    }
                } else {
                    $code = 400;
                    $status = $this->Response->responseFormat($code);
                    $status['error'] = $user->getError('new_password');
                }
            } else {
                $code = 401;
                $status = $this->Response->responseFormat($code);
                $status['message'] = 'Email token already used.';
            }
            $this->response = $this->response->withStatus($status['code']);
            $this->set(compact(['status']));
            $this->viewBuilder()->setOption('serialize', ['status']);
        }
    }

    public function logout()
    {
        $this->Authorization->skipAuthorization();
        $this->request->allowMethod(['post']);
        $result = $this->Authentication->getResult();
        if ($result->isValid()) {
            $this->Authentication->logout();
            $code = 200;
            $status = $this->Response->responseFormat($code);
            $this->response = $this->response->withStatus($status['code']);
            $this->set(compact(['status']));
            $this->viewBuilder()->setOption('serialize', ['status']);
        } else {
            $code = 500;
            $status = $this->Response->responseFormat($code);
            $this->response = $this->response->withStatus($status['code']);
            $this->set(compact(['status']));
            $this->viewBuilder()->setOption('serialize', ['status']);
        }
    }

    public function userInfo()
    {
        $this->request->allowMethod(['get']);
        $user = $this->Authentication->getIdentity();

        $retweetConditions = [
            'OR' => [
                'Retweets.user_id' => $user['id'],
                [
                    'Follower.follower_user_id' => $user['id'],
                    'Follower.deleted' => 0
                ]
            ],
            'Retweets.deleted' => 0
        ];
        $retweet = $this->Retweets->find(
            'all',
            [
                'conditions' => $retweetConditions,
                'join' => [
                    [
                        'table' => 'followers',
                        'alias' => 'Follower',
                        'type' => 'LEFT',
                        'conditions' => 'Follower.user_id = Retweets.user_id',
                    ],
                ],
                'order' => ['Retweets.modified' => 'desc'],
                'contain' => ['Posts', 'Users', 'Followers']
            ]
        );

        $followingCond = [
            'Followers.follower_user_id' => $user['id'],
            'Followers.deleted' => 0
        ];
        $following = $this->Followers->find(
            'all',
            [
                'conditions' => $followingCond,
                'order' => array('Followers.modified' => 'desc'),
                'limit' => 5,
                'contain' => ['UsersFollowing', 'UsersFollowers']
            ]
        );
        $followingQuery = $this->Followers->find(
            'all',
            [
                'conditions' => $followingCond,
                'order' => ['Followers.modified' => 'desc'],
                'contain' => ['UsersFollowing', 'UsersFollowers']
            ]
        );
        $following_count = $followingQuery->count();

        $followersCond = [
            'Followers.user_id' => $user['id'],
            'Followers.deleted' => 0
        ];
        $followers = $this->Followers->find(
            'all',
            [
                'conditions' => $followersCond,
                'order' => ['Followers.modified' => 'desc'],
                'limit' => 5,
                'contain' => ['UsersFollowing', 'UsersFollowers']
            ]
        );
        $followerQuery = $this->Followers->find(
            'all',
            [
                'conditions' => $followersCond,
                'contain' => ['UsersFollowing', 'UsersFollowers']
            ]
        );
        $follower_count = $followerQuery->count();

        $likeConditions = [
            'PostLikes.user_id' => $user['id'],
            'PostLikes.deleted' => 0
        ];
        $likeQuery = $this->PostLikes->find(
            'all',
            [
                'conditions' => $likeConditions,
                'contain' => 'Users', 'Posts'
            ]
        );
        $like_count = $likeQuery->count();

        $post = $this->Posts->newEmptyEntity();
        $this->Authorization->authorize($post);

        $code = 200;
        $status = $this->Response->responseFormat($code);
        $this->response = $this->response->withStatus($status['code']);
        $this->set(compact([
            'status', 'retweet', 'following', 'followers', 'following_count', 'follower_count', 'like_count'
        ]));
        $this->viewBuilder()->setOption('serialize', [
            'status', 'retweet', 'following', 'followers', 'following_count', 'follower_count', 'like_count'
        ]);
    }

    public function unauthenticated()
    {
        $this->Authorization->skipAuthorization();
        $header = $this->request->getHeaderLine('Authorization');
        $result = $this->Authentication->getResult();
        //if ($result->isValid()) {
            if (!empty($header)) {
                $userInfo = $this->JWTTokens->checkToken($header);
                if (empty($userInfo)) {
                    $error = 'Authorization token is invalid.';
                } else {
                    $error = 'Authorization token already expired.';
                }
            } else {
                throw new UnauthorizedException();
            }
        //} else {
            //$error = 'You need to login to access this application.';
        //}
        $code = 401;
        $status = $this->Response->responseFormat($code);
        $status['error'] = $error;
        $this->response = $this->response->withStatus($code);
        $this->set(compact(['status']));
        $this->viewBuilder()->setOption('serialize', ['status']);
        
    }
}