<?php

declare(strict_types=1);

namespace App\Controller\Api;

use App\Controller\AppController;
use Cake\Http\Exception\ConflictException;
use Cake\Http\Exception\NotFoundException;
use Cake\Http\Exception\UnauthorizedException;
use Cake\I18n\FrozenTime;
use Cake\Mailer\Mailer;
use Cake\Utility\Security;
use Firebase\JWT\JWT;

/**
 * Users Controller
 *
 * @property \App\Model\Table\UsersTable $Users
 * @method \App\Model\Entity\User[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class RetweetsController extends AppController
{
	public function initialize(): void
    {
        parent::initialize();

        $this->loadComponent('Paginator');
        $this->loadComponent('RequestHandler');
        $this->loadComponent('MyAuth');

        $this->RequestHandler->renderAs($this, 'json');
    }

    public function retweet($id)
    {
        $this->request->allowMethod(['post']);
        $result = $this->Posts->findById($id)
            ->first();
        if (empty($result)) {
            throw new NotFoundException();
        }
        $user = $this->Authentication->getIdentity();
        $post = $this->Posts->get($id);
        $this->Authorization->authorize($post);
        $headerAuth = $this->request->getHeaderLine('Authorization');
        $header = $this->JWTTokens->checkHeaderAuth($headerAuth);
        if (isset($header)) {
            $userInfo = $this->JWTTokens->checkToken($header);
        }
        if (!empty($userInfo)) {
            $conditions = [
                'Retweets.user_id' => $user['id'],
                'Retweets.post_id' => $post['id']
            ];
            $retweetPost = $this->Retweets->find('all');
            $retweet = $retweetPost->first();

            if ($retweet['deleted'] == 1) {
                $retweet['deleted'] = 0;
                $retweet['modified'] = FrozenTime::now('Asia/Hong_Kong');
                if ($this->Retweets->save($retweet)) {
                    $code = 201;
                    $status = $this->Response->responseFormat($code);
                } else {
                    $code = 403;
                    $status = $this->Response->responseFormat($code);
                }
                $this->response = $this->response->withStatus($status['code']);
                $this->set(compact(['status', 'retweet']));
                $this->viewBuilder()->setOption('serialize', ['status', 'retweet']);
            } else {
                $post_retweet = $this->Retweets->newEmptyEntity();
                $post_retweet['post_id'] = $post['id'];
                $post_retweet['user_id'] = $user['id'];
                if ($this->Retweets->save($post_retweet)) {
                    $code = 201;
                    $status = $this->Response->responseFormat($code);
                } else {
                    $code = 403;
                    $status = $this->Response->responseFormat($code);
                }
                $this->response = $this->response->withStatus($status['code']);
                $this->set(compact(['status', 'post_retweet']));
                $this->viewBuilder()->setOption('serialize', ['status', 'post_retweet']);
            }
        } else {
            $code = 401;
            $status = $this->Response->responseFormat($code);
            $status['error'] = 'Authorization token is not valid.';
            $this->response = $this->response->withStatus($code);
            $this->set(compact(['status']));
            $this->viewBuilder()->setOption('serialize', ['status']);
        }
    }

    public function delete($id = null)
    {
        $this->request->allowMethod(['delete']);
        $result = $this->Retweets->findById($id)
            ->first();
        if (empty($result)) {
            throw new NotFoundException();
        }
        $retweet = $this->Retweets->get($id);
        $this->Authorization->authorize($retweet);
        $headerAuth = $this->request->getHeaderLine('Authorization');
        $header = $this->JWTTokens->checkHeaderAuth($headerAuth);
        if (isset($header)) {
            $userInfo = $this->JWTTokens->checkToken($header);
        }
        if (!empty($userInfo)) {
            if ($retweet['deleted'] == 0) {
                $retweet['deleted'] = 1;
                if ($this->Retweets->save($retweet)) {
                    $code = 200;
                    $status = $this->Response->responseFormat($code);
                } else {
                    $code = 401;
                    $status = $this->Response->responseFormat($code);
                }
            } else {
                $code = 404;
                $status = $this->Response->responseFormat($code);
            }
            $this->response = $this->response->withStatus($status['code']);
            $this->set(compact(['status']));
            $this->viewBuilder()->setOption('serialize', ['status']);
        } else {
            $code = 401;
            $status = $this->Response->responseFormat($code);
            $status['error'] = 'Authorization token is not valid.';
            $this->response = $this->response->withStatus($code);
            $this->set(compact(['status']));
            $this->viewBuilder()->setOption('serialize', ['status']);
        }
    }
}