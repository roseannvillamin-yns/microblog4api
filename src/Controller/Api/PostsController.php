<?php

declare(strict_types=1);

namespace App\Controller\Api;

use App\Controller\AppController;
use Cake\Http\Exception\ConflictException;
use Cake\Http\Exception\NotFoundException;
use Cake\Http\Exception\UnauthorizedException;
use Cake\I18n\FrozenTime;
use Cake\Mailer\Mailer;
use Cake\Utility\Security;
use Firebase\JWT\JWT;

/**
 * Users Controller
 *
 * @property \App\Model\Table\UsersTable $Users
 * @method \App\Model\Entity\User[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class PostsController extends AppController
{
	public function initialize(): void
    {
        parent::initialize();

        $this->loadComponent('Paginator');
        $this->loadComponent('RequestHandler');
        $this->loadComponent('MyAuth');

        $this->RequestHandler->renderAs($this, 'json');
    }

    public function index()
    {
        $status = array();
        $this->request->allowMethod(['get']);
        $post = $this->Posts->newEmptyEntity();
        $this->Authorization->authorize($post);
        $this->set(compact('post'));
        $headerAuth = $this->request->getHeaderLine('Authorization');
        $header = $this->JWTTokens->checkHeaderAuth($headerAuth);
        if (isset($header)) {
            $userInfo = $this->JWTTokens->checkToken($header);
        }
        
        $user = $this->Authentication->getIdentity();
        if (!empty($userInfo)) {
            $postsCondition = [
                'OR' => [
                    'Posts.user_id' => $user['id'],
                    [
                        'Follower.follower_user_id' => $user['id'],
                        'Follower.deleted' => 0
                    ]
                ],
                'Posts.deleted' => 0
            ];
            $page_settings = [
                'conditions' => $postsCondition,
                'join' => [
                    [
                        'table' => 'followers',
                        'alias' => 'Follower',
                        'type' => 'LEFT',
                        'conditions' => 'Follower.user_id = Posts.user_id',
                    ],
                ],
                'group' => ['Posts.id'],
                'contain' => ['Users'],
                'limit' => 10,
                'order' => [
                    'Posts.created' => 'desc'
                ]
            ];
            $posts = $this->Paginator->paginate($this->Posts, $page_settings);
            $page = $this->request->getAttribute('paging');

            $likeConditions = [
                'PostLikes.user_id' => $user['id'],
                'PostLikes.deleted' => 0
            ];
            $likes = $this->PostLikes->find(
                'all',
                [
                    'conditions' => $likeConditions,
                    'contain' => 'Users', 'Posts',
                    'order' => ['PostLikes.created' => 'desc'],
                ]
            );

            $code = 200;
            $status = $this->Response->responseFormat($code);
            $this->response = $this->response->withStatus($code);
            $this->set(compact(['status', 'posts', 'likes', 'page_settings']));
            $this->viewBuilder()->setOption('serialize', ['status', 'posts', 'likes', 'page_settings']);
        } else {
            $code = 401;
            $status = $this->Response->responseFormat($code);
            $status['error'] = 'Authorization token is not valid.';
            $this->response = $this->response->withStatus($code);
            $this->set(compact(['status']));
            $this->viewBuilder()->setOption('serialize', ['status']);
        }
    }

    public function add()
    {
        $status = array();
        $this->request->allowMethod(['post']);
        $post = $this->Posts->newEmptyEntity();
        $this->Authorization->authorize($post);
        $headerAuth = $this->request->getHeaderLine('Authorization');
        $header = $this->JWTTokens->checkHeaderAuth($headerAuth);
        if (isset($header)) {
            $userInfo = $this->JWTTokens->checkToken($header);
        }
        if (!empty($userInfo)) {
            if ($this->request->is('post')) {
                $post = $this->Posts->patchEntity($post, $this->request->getData());
                $image = $this->request->getData('image_file')->getClientFilename();
                $post['image'] = $image;
                if (!$post->getErrors()) {
                    if ($this->Posts->save($post)) {
                    	if (!empty($image)) {
                    		$targetPath = WWW_ROOT . 'img' . DS . 'postImages' . DS . $image;
                    		$this->request->getData('image_file')->moveTo($targetPath);
                    	}
                        $code = 201;
                        $status = $this->Response->responseFormat($code);
                    }
                } else {
                    $code = 400;
                    $status = $this->Response->responseFormat($code);
                    $status['error'] = $post->getErrors();
                }

                $this->response = $this->response->withStatus($status['code']);
                $this->set(compact(['status', 'post']));
                $this->viewBuilder()->setOption('serialize', ['status', 'post']);
            }
        } else {
            $code = 401;
            $status = $this->Response->responseFormat($code);
            $status['error'] = 'Authorization token is not valid.';
            $this->response = $this->response->withStatus($code);
            $this->set(compact(['status']));
            $this->viewBuilder()->setOption('serialize', ['status']);
        }
    }

    public function edit($id = null)
    {
        $this->request->allowMethod(['put']);
        $headerAuth = $this->request->getHeaderLine('Authorization');
        $header = $this->JWTTokens->checkHeaderAuth($headerAuth);
        $user = $this->Users->findByAccessToken($header)->first();
        $result = $this->Posts->findById($id)
            ->first();
        if (empty($result)) {
            throw new ConflictException(__('Post not found.'));
        } elseif ($user['id'] != $result['user_id']) {
            throw new UnauthorizedException();
        }
        $post = $this->Posts->get($id);
        $this->Authorization->authorize($post);
        if (isset($header)) {
            $userInfo = $this->JWTTokens->checkToken($header);
        }
        if (!empty($userInfo)) {
            if ($post['deleted'] == 0) {
                if ($this->request->is(['put'])) {
                    $post = $this->Posts->patchEntity($post, $this->request->getData());
                    $image = $this->request->getData('image_file')->getClientFilename();
                    $post['image'] = $image;
                    if (!$post->getErrors()) {
                        if ($this->Posts->save($post)) {
                        	if (!empty($image)) {
                        		$targetPath = WWW_ROOT . 'img' . DS . 'postImages' . DS . $image;
                        		$this->request->getData('image_file')->moveTo($targetPath);
                        	}
                            $code = 200;
                            $status = $this->Response->responseFormat($code);
                        } else {
                            $code = 401;
                            $status = $this->Response->responseFormat($code);
                        }
                    } else {
                        $code = 400;
                        $status = $this->Response->responseFormat($code);
                        $status['error'] = $post->getErrors();
                    }
                    $this->response = $this->response->withStatus($status['code']);
                    $this->set(compact(['status', 'post']));
                    $this->viewBuilder()->setOption('serialize', ['status', 'post']);
                }
            } else {
                $code = 400;
                $status = $this->Response->responseFormat($code);
                $this->response = $this->response->withStatus($status['code']);
                $this->set(compact(['status']));
                $this->viewBuilder()->setOption('serialize', ['status']);
            }
        } else {
            $code = 401;
            $status = $this->Response->responseFormat($code);
            $status['error'] = 'Authorization token is not valid.';
            $this->set(compact(['status']));
            $this->viewBuilder()->setOption('serialize', ['status']);
        }
    }

    public function delete($id = null)
    {
        $status = array();
        $this->request->allowMethod(['delete']);
        $headerAuth = $this->request->getHeaderLine('Authorization');
        $header = $this->JWTTokens->checkHeaderAuth($headerAuth);
        $user = $this->Users->findByAccessToken($header)->first();
        $result = $this->Posts->findById($id)
            ->first();
        if (empty($result)) {
            throw new NotFoundException(__('Record not found.'));
        } elseif ($user['id'] != $result['user_id']) {
            throw new UnauthorizedException(__('Unauthorized.'));
        }
        
        $post = $this->Posts->get($id);
        $this->Authorization->authorize($post);
        if (isset($header)) {
            $userInfo = $this->JWTTokens->checkToken($header);
        }
        if (!empty($userInfo)) {
            $post['deleted'] = 1;
            $post['deleted_date'] = FrozenTime::now('Asia/Hong_Kong');
            if ($this->Posts->save($post)) {
                $code = 200;
                $status = $this->Response->responseFormat($code);
            } else {
                $code = 401;
                $status = $this->Response->responseFormat($code);
            }
            $this->response = $this->response->withStatus($status['code']);
            $this->set(compact(['status', 'post']));
            $this->viewBuilder()->setOption('serialize', ['status', 'post']);
        } else {
            $code = 401;
            $status = $this->Response->responseFormat($code);
            $status['error'] = 'Authorization token is not valid.';
            $this->response = $this->response->withStatus($code);
            $this->set(compact(['status']));
            $this->viewBuilder()->setOption('serialize', ['status']);
        }
    }

    public function search()
    {
        $this->request->allowMethod(['get']);
        $post = $this->Posts->newEmptyEntity();
        $this->Authorization->authorize($post);
        $user = $this->Authentication->getIdentity();
        $headerAuth = $this->request->getHeaderLine('Authorization');
        $header = $this->JWTTokens->checkHeaderAuth($headerAuth);
        if (isset($header)) {
            $userInfo = $this->JWTTokens->checkToken($header);
        }
        if (!empty($userInfo)) {
            if (!empty($this->request->getQuery('keyword'))) {
                $keyword = $this->request->getQuery('keyword');
                $conditions = [
                    'OR' => [
                        'Users.full_name LIKE' => '%' . $keyword . '%',
                        'Users.username LIKE' => '%' . $keyword . '%',
                        'Posts.post LIKE' => '%' . $keyword . '%'
                    ],
                    'Users.verified' => 1,
                    'Posts.deleted' => 0
                ];
            } else {
                $conditions = '';
            }

            $page_settings = [
                'conditions' => $conditions,
                'contain' => ['Users'],
                'limit' => 5,
                'order' => [
                    'Posts.created' => 'desc'
                ]
            ];
            $results = $this->Paginator->paginate($this->Posts, $page_settings);
            $page = $this->request->getAttribute('paging');

            $likeConditions = [
                'PostLikes.user_id' => $user['id'],
                'PostLikes.deleted' => 0
            ];
            $likes = $this->PostLikes->find(
                'all',
                [
                    'conditions' => $likeConditions,
                    'contain' => 'Users', 'Posts',
                    'order' => ['PostLikes.created' => 'desc'],
                ]
            );

            if (!empty($conditions)) {
                $code = 200;
                $status = $this->Response->responseFormat($code);
                $this->response = $this->response->withStatus($status['code']);
                $this->set(compact(['status', 'results', 'likes', 'page_settings']));
                $this->viewBuilder()->setOption('serialize', ['status', 'results', 'likes', 'page_settings']);
            } else {
                $code = 400;
                $status = $this->Response->responseFormat($code);
                $this->response = $this->response->withStatus($status['code']);
                $this->set(compact(['status']));
                $this->viewBuilder()->setOption('serialize', ['status']);
            }
        } else {
            $code = 401;
            $status = $this->Response->responseFormat($code);
            $status['error'] = 'Authorization token is not valid.';
            $this->response = $this->response->withStatus($code);
            $this->set(compact(['status']));
            $this->viewBuilder()->setOption('serialize', ['status']);
        }
    }
}