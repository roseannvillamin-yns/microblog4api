<?php

declare(strict_types=1);

namespace App\Controller\Api;

use App\Controller\AppController;
use Cake\Http\Exception\ConflictException;
use Cake\Http\Exception\NotFoundException;
use Cake\Http\Exception\UnauthorizedException;
use Cake\I18n\FrozenTime;
use Cake\Mailer\Mailer;
use Cake\Utility\Security;
use Firebase\JWT\JWT;

/**
 * Users Controller
 *
 * @property \App\Model\Table\UsersTable $Users
 * @method \App\Model\Entity\User[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class PostLikesController extends AppController
{
	public function initialize(): void
    {
        parent::initialize();

        $this->loadComponent('Paginator');
        $this->loadComponent('RequestHandler');
        $this->loadComponent('MyAuth');

        $this->RequestHandler->renderAs($this, 'json');
    }

    public function like($id)
    {
        $this->request->allowMethod(['post']);
        $headerAuth = $this->request->getHeaderLine('Authorization');
        $header = $this->JWTTokens->checkHeaderAuth($headerAuth);
        $result = $this->Posts->findById($id)
            ->first();
        if (empty($result)) {
            throw new NotFoundException();
        }
        $post = $this->Posts->get($id);
        $user = $this->Authentication->getIdentity();
        $userAuthorized = $this->Users->get($user['id']);
        $this->Authorization->authorize($userAuthorized);
        if (isset($header)) {
            $userInfo = $this->JWTTokens->checkToken($header);
        }
        if (!empty($userInfo)) {
            if ($post != null) {
                $conditions = [
                    'PostLikes.user_id' => $user['id'],
                    'PostLikes.post_id' => $post['id']
                ];
                $likePost = $this->PostLikes->find('all', [
                    'conditions' => $conditions
                ]);
                $like = $likePost->first();

                if (!empty($like)) {
                    if ($like['deleted'] == 1) {
                        $like['deleted'] = 0;
                        $like['modified'] = FrozenTime::now('Asia/Hong_Kong');
                        $this->PostLikes->save($like);
                        $code = 200;
                        $status = $this->Response->responseFormat($code);
                    } else {
                        $code = 403;
                        $status = $this->Response->responseFormat($code);
                    }
                    $this->response = $this->response->withStatus($status['code']);
                    $this->set(compact(['status', 'like']));
                    $this->viewBuilder()->setOption('serialize', ['status', 'like']);
                } else {
                    $post_like = $this->PostLikes->newEmptyEntity();
                    $post_like['post_id'] = $post['id'];
                    $post_like['user_id'] = $user['id'];
                    if ($this->PostLikes->save($post_like)) {
                        $code = 200;
                        $status = $this->Response->responseFormat($code);
                    } else {
                        $code = 403;
                        $status = $this->Response->responseFormat($code);
                    }
                    $this->response = $this->response->withStatus($status['code']);
                    $this->set(compact(['status', 'post_like']));
                    $this->viewBuilder()->setOption('serialize', ['status', 'post_like']);
                }
            }
        } else {
            $code = 401;
            $status = $this->Response->responseFormat($code);
            $status['error'] = 'Authorization token is not valid.';
            $this->response = $this->response->withStatus($code);
            $this->set(compact(['status']));
            $this->viewBuilder()->setOption('serialize', ['status']);
        }
    }

    public function dislike($id)
    {
        $this->request->allowMethod(['delete']);
        $headerAuth = $this->request->getHeaderLine('Authorization');
        $header = $this->JWTTokens->checkHeaderAuth($headerAuth);
        $result = $this->PostLikes->findById($id)
            ->first();
        if (empty($result)) {
            throw new NotFoundException();
        }
        $post = $this->Posts->get($id);
        $user = $this->Authentication->getIdentity();
        $userAuthorized = $this->Users->get($user['id']);
        $this->Authorization->authorize($userAuthorized);
        if (isset($header)) {
            $userInfo = $this->JWTTokens->checkToken($header);
        }
        if (!empty($userInfo)) {
            if ($post != null) {
                $conditions = array(
                    'PostLikes.post_id' => $post['id'],
                    'PostLikes.user_id' => $user['id']
                );

                $dislikePost = $this->PostLikes->find('all', [
                    'conditions' => $conditions
                ]);
                $dislike = $dislikePost->first();

                if (!empty($dislike)) {
                    if ($dislike['deleted'] == 0) {
                        $dislike['deleted'] = 1;
                        $dislike['modified'] = FrozenTime::now('Asia/Hong_Kong');
                        $this->PostLikes->save($dislike);
                        $code = 200;
                        $status = $this->Response->responseFormat($code);
                    } else {
                        $code = 403;
                        $status = $this->Response->responseFormat($code);
                    }
                    $this->response = $this->response->withStatus($status['code']);
                    $this->set(compact(['status', 'dislike']));
                    $this->viewBuilder()->setOption('serialize', ['status', 'dislike']);
                } else {
                    $code = 404;
                    $status = $this->Response->responseFormat($code);
                    $this->response = $this->response->withStatus($status['code']);
                    $this->set(compact(['status']));
                    $this->viewBuilder()->setOption('serialize', ['status']);
                }
            }
        } else {
            $code = 401;
            $status = $this->Response->responseFormat($code);
            $status['error'] = 'Authorization token is not valid.';
            $this->response = $this->response->withStatus($code);
            $this->set(compact(['status']));
            $this->viewBuilder()->setOption('serialize', ['status']);
        }
    }
}