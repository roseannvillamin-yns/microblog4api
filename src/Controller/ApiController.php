<?php

declare(strict_types=1);

namespace App\Controller;

use App\Controller\AppController;
use Cake\I18n\FrozenTime;
use Cake\Mailer\Mailer;
use Cake\Utility\Security;
use Firebase\JWT\JWT;
use Cake\Http\Exception\UnauthorizedException;
use Cake\Http\Exception\NotFoundException;
use Cake\Http\Exception\ConflictException;

class ApiController extends AppController
{
    public function initialize(): void
    {
        parent::initialize();

        $this->loadComponent('Paginator');
        $this->loadComponent('Flash');
        $this->loadComponent('RequestHandler');
        $this->loadComponent('MyAuth');

        $this->RequestHandler->renderAs($this, 'json');
    }

    public function beforeFilter(\Cake\Event\EventInterface $event)
    {
        parent::beforeFilter($event);
        // Configure the login action to not require authentication, preventing
        // the infinite redirect loop issue
        $this->Authentication->addUnauthenticatedActions([
            'login', 'add', 'verification', 'logout', 'forgotPassword', 'changePassword', 'unauthenticated'
        ]);
    }

    public function userInfo()
    {
        $this->request->allowMethod(['get']);
        $user = $this->Authentication->getIdentity();

        $retweetConditions = [
            'OR' => [
                'Retweets.user_id' => $user['id'],
                [
                    'Follower.follower_user_id' => $user['id'],
                    'Follower.deleted' => 0
                ]
            ],
            'Retweets.deleted' => 0
        ];
        $retweet = $this->Retweets->find(
            'all',
            [
                'conditions' => $retweetConditions,
                'join' => [
                    [
                        'table' => 'followers',
                        'alias' => 'Follower',
                        'type' => 'LEFT',
                        'conditions' => 'Follower.user_id = Retweets.user_id',
                    ],
                ],
                'order' => ['Retweets.modified' => 'desc'],
                'contain' => ['Posts', 'Users', 'Followers']
            ]
        );

        $followingCond = [
            'Followers.follower_user_id' => $user['id'],
            'Followers.deleted' => 0
        ];
        $following = $this->Followers->find(
            'all',
            [
                'conditions' => $followingCond,
                'order' => array('Followers.modified' => 'desc'),
                'limit' => 5,
                'contain' => ['UsersFollowing', 'UsersFollowers']
            ]
        );
        $followingQuery = $this->Followers->find(
            'all',
            [
                'conditions' => $followingCond,
                'order' => ['Followers.modified' => 'desc'],
                'contain' => ['UsersFollowing', 'UsersFollowers']
            ]
        );
        $following_count = $followingQuery->count();

        $followersCond = [
            'Followers.user_id' => $user['id'],
            'Followers.deleted' => 0
        ];
        $followers = $this->Followers->find(
            'all',
            [
                'conditions' => $followersCond,
                'order' => ['Followers.modified' => 'desc'],
                'limit' => 5,
                'contain' => ['UsersFollowing', 'UsersFollowers']
            ]
        );
        $followerQuery = $this->Followers->find(
            'all',
            [
                'conditions' => $followersCond,
                'contain' => ['UsersFollowing', 'UsersFollowers']
            ]
        );
        $follower_count = $followerQuery->count();

        $likeConditions = [
            'PostLikes.user_id' => $user['id'],
            'PostLikes.deleted' => 0
        ];
        $likeQuery = $this->PostLikes->find(
            'all',
            [
                'conditions' => $likeConditions,
                'contain' => 'Users', 'Posts'
            ]
        );
        $like_count = $likeQuery->count();

        $post = $this->Posts->newEmptyEntity();
        $this->Authorization->authorize($post);

        $code = 200;
        $status = $this->Response->responseFormat($code);
        $this->response = $this->response->withStatus($status['code']);
        $this->set(compact([
            'status', 'retweet', 'following', 'followers', 'following_count', 'follower_count', 'like_count'
        ]));
        $this->viewBuilder()->setOption('serialize', [
            'status', 'retweet', 'following', 'followers', 'following_count', 'follower_count', 'like_count'
        ]);
    }

    // User's Login
    public function login()
    {
        $status = array();
        $this->Authorization->skipAuthorization();
        $this->request->allowMethod(['post']);
        if ($code = 500) {
            $code = 500;
            $status = $this->Response->responseFormat($code);
            $this->response = $this->response->withStatus($status['code']);
            $this->set(compact(['status']));
            $this->viewBuilder()->setOption('serialize', ['status']);
        }
        $password = $this->request->getData('password');

        if ($this->request->getData('username') != null) {
            $user = $this->Users
                ->findByUsername($this->request->getData('username'))
                ->first();
            $userPass = $user['password'];
            if ($user != null) {
                $result = $this->MyAuth->checkUser($password, $userPass);
                if ($result == true && $user['verified'] == 1) {
                    $id = $user['id'];
                    $json = $this->JWTTokens->generateAccessToken($id);
                    $access_token = $json['token'];
                    $user['access_token'] = $access_token;
                    if ($this->Users->save($user)) {
                        $code = 200;
                        $status = $this->Response->responseFormat($code);
                        $json = $user['access_token'];
                    }
                    $this->response = $this->response->withStatus($status['code']);
                    $this->set(compact(['status', 'user']));
                    $this->viewBuilder()->setOption('serialize', ['status', 'user']);
                } else {
                    $code = 401;
                    $status = $this->Response->responseFormat($code);
                    $status['error'] = 'Invalid username or password.';
                    $this->response = $this->response->withStatus($status['code']);
                    $this->set(compact(['status']));
                    $this->viewBuilder()->setOption('serialize', ['status']);
                }
            } else {
                $code = 401;
                $status = $this->Response->responseFormat($code);
                $status['error'] = 'Invalid username or password.';
                $this->response = $this->response->withStatus($status['code']);
                $this->set(compact(['status']));
                $this->viewBuilder()->setOption('serialize', ['status']);
            }
        }
    }

    // User's Add
    public function add()
    {
        $this->Authorization->skipAuthorization();
        $user = $this->Users->newEmptyEntity();
        $this->request->allowMethod(['post']);
        if ($this->request->is('post')) {
            $code = md5($this->request->getData('username'));
            $user['email_token'] = $code;
            $user['created'] = FrozenTime::now('Asia/Hong_Kong');
            $recipient = $this->request->getData('email');
            $username = $this->request->getData('username');
            $user = $this->Users->patchEntity($user, $this->request->getData());
            if (!$user->getErrors()) {
                if ($this->Users->save($user)) {
                    $body = 'To complete the registration, kindly click the link for activation: ';
                    $link = 'http://localhost/microblog4api/users/verification/' . $username . '/' . $code;
                    $message = $body . $link;
                    $mailer = new Mailer();
                    $mailer->setFrom(['cakemicroblog@gmail.com' => 'Microblog CakePHP 4'])
                        ->setTo($recipient)
                        ->setSubject('Account Activation')
                        ->deliver($message);

                    $code = 201;
                    $status = $this->Response->responseFormat($code);
                }
            } else {
                $code = 400;
                $status = $this->Response->responseFormat($code);
                $status['error'] = $user->getErrors();
            }
            $this->response = $this->response->withStatus($status['code']);
            $this->set(compact(['status']));
            $this->viewBuilder()->setOption('serialize', ['status']);
        }
    }

    // User's Verification
    public function verification($username, $code)
    {
        $this->Authorization->skipAuthorization();
        $this->request->allowMethod(['patch']);
        $user = $this->Users
                ->findByUsername($username)
                ->first();
        if ($user['verified'] == 0 && $user['email_token'] === $code) {
            $user['verified'] = 1;
            $this->Users->save($user);
            $code = 200;
            $status = $this->Response->responseFormat($code);
        } else {
            $code = 404;
            $status = $this->Response->responseFormat($code);
        }
        $this->response = $this->response->withStatus($status['code']);
        $this->set(compact(['status']));
        $this->viewBuilder()->setOption('serialize', ['status']);
    }

    // User's View
    public function viewUser($username = null)
    {
        $this->RequestHandler->renderAs($this, 'json');
        $status = array();
        $this->request->allowMethod(['get']);
        $userData = $this->Authentication->getIdentity();
        $result = $this->Users
                ->findByUsername($username)
                ->first();
        if (empty($result)) {
            throw new NotFoundException(__('User not found.'));
        }

        $user = $this->Users->get($result['id']);
        $this->Authorization->authorize($user);
        $headerAuth = $this->request->getHeaderLine('Authorization');
        $header = $this->JWTTokens->checkHeaderAuth($headerAuth);
        if (isset($header)) {
            $userInfo = $this->JWTTokens->checkToken($header);
        }

        if (!empty($userInfo)) {
            $conditions = [
                'Posts.deleted' => 0,
                'Posts.user_id' => $user['id']
            ];

            $page_settings = [
                'conditions' => $conditions,
                'contain' => 'Users',
                'limit' => 10,
                'order' => [
                    'Posts.created' => 'desc'
                ]
            ];

            $posts = $this->Paginator->paginate($this->Posts, $page_settings);
            $page = $this->request->getAttribute('paging');

            $likeConditions = [
                'PostLikes.user_id' => $userData['id'],
                'PostLikes.deleted' => 0
            ];
            $likes = $this->PostLikes->find(
                'all',
                [
                    'conditions' => $likeConditions,
                    'contain' => 'Users', 'Posts',
                    'order' => ['PostLikes.created' => 'desc'],
                ]
            );

            $this->set('likes', $likes);

            if ($result != null) {
                $code = 200;
                $status = $this->Response->responseFormat($code);
            }

            $this->response = $this->response->withStatus($status['code']);
            $this->set(compact([
                'status', 'user', 'posts', 'likes', 'page_settings'
            ]));
            $this->viewBuilder()->setOption('serialize', [
                'status',  'user', 'posts', 'likes', 'page_settings'
            ]);
        } else {
            $code = 401;
            $status = $this->Response->responseFormat($code);
            $status['error'] = 'Authorization token is not valid.';
            $this->response = $this->response->withStatus($code);
            $this->set(compact(['status']));
            $this->viewBuilder()->setOption('serialize', ['status']);
        }
    }

    //User's Edit
    public function editUser($id = null)
    {
        $status = array();
        $this->request->allowMethod(['put']);
        /*$header = $this->request->getHeaderLine('Authorization');
        if (empty($header)) {
            throw new UnauthorizedException();
        }*/
        $headerAuth = $this->request->getHeaderLine('Authorization');
        $header = $this->JWTTokens->checkHeaderAuth($headerAuth);
        $userLogged = $this->Users->findByAccessToken($header)->first();
        $result = $this->Users->findById($id)
            ->first();
        if (empty($result)) {
            throw new NotFoundException(__('User not found.'));
        } elseif ($userLogged['id'] != $result['id']) {
            throw new UnauthorizedException(__('Unauthorized.'));
        }
        $user = $this->Users->get($id);
        $this->Authorization->authorize($user);
        if (isset($header)) {
            $userInfo = $this->JWTTokens->checkToken($header);
        }
        if (!empty($userInfo)) {
            if ($this->request->is(['put'])) {
                $user = $this->Users->patchEntity($user, $this->request->getData());
                $user['password'] = $this->request->getData('new_password');
                if (!$user->getErrors()) {
                    if ($this->Users->save($user)) {
                        $code = 200;
                        $status = $this->Response->responseFormat($code);
                    }
                } else {
                    $code = 400;
                    $status = $this->Response->responseFormat($code);
                    $status['error'] = $user->getErrors();
                }
                $this->response = $this->response->withStatus($status['code']);
                $this->set(compact(['status', 'user']));
                $this->viewBuilder()->setOption('serialize', ['status', 'user']);
            }
        } else {
            $code = 401;
            $status = $this->Response->responseFormat($code);
            $status['error'] = 'Authorization token is not valid.';
            $this->response = $this->response->withStatus($code);
            $this->set(compact(['status']));
            $this->viewBuilder()->setOption('serialize', ['status']);
        }
    }

    // Users All Users
    public function viewUsers()
    {
        $status = array();
        $this->request->allowMethod(['get']);
        /*$header = $this->request->getHeaderLine('Authorization');
        if (empty($header)) {
            throw new UnauthorizedException();
        }*/
        $headerAuth = $this->request->getHeaderLine('Authorization');
        $header = $this->JWTTokens->checkHeaderAuth($headerAuth);
        $user = $this->Authentication->getIdentity();
        $userAuthorized = $this->Users->get($user['id']);
        $this->Authorization->authorize($userAuthorized);
        if (isset($header)) {
            $userInfo = $this->JWTTokens->checkToken($header);
        }
        if (!empty($userInfo)) {
            $userConditions = [
                'Users.verified' => 1
            ];
            $page_settings = [
                'conditions' => $userConditions,
                'limit' => 10,
                'order' => [
                    'created' => 'desc'
                ]
            ];
            $users = $this->Paginator->paginate($this->Users, $page_settings);
            $page = $this->request->getAttribute('paging');

            $code = 200;
            $status = $this->Response->responseFormat($code);
            $this->response = $this->response->withStatus($status['code']);
            $this->set(compact(['status', 'users', 'page_settings']));
            $this->viewBuilder()->setOption('serialize', ['status', 'users', 'page_settings']);
        } else {
            $code = 401;
            $status = $this->Response->responseFormat($code);
            $status['error'] = 'Authorization token is not valid.';
            $this->response = $this->response->withStatus($code);
            $this->set(compact(['status']));
            $this->viewBuilder()->setOption('serialize', ['status']);
        }
    }

    // User's Forgot Password
    public function forgotPassword()
    {
        $status = array();
        $this->request->allowMethod(['put']);
        $this->Authorization->skipAuthorization();
        if ($this->request->is(['put'])) {
            $user = $this->Users
                ->findByEmail($this->request->getData('email'))
                ->first();
            if (!empty($user) && $user['verified'] == 1) {
                $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
                $code = substr(str_shuffle($characters), 0, 32);
                $user['verified'] = 0;
                $user['email_token'] = $code;
                $recipient = $user['email'];
                $username = $user['username'];
                $user['modified'] = FrozenTime::now('Asia/Hong_Kong');

                if ($this->Users->save($user)) {
                    $body = 'Kindly click the link to change password: ';
                    $link = 'http://localhost/microblog4api/users/changePassword/' . $code;
                    $message = $body . $link;
                    $mailer = new Mailer();
                    $mailer->setFrom(['cakemicroblog@gmail.com' => 'Microblog CakePHP 4'])
                        ->setTo($recipient)
                        ->setSubject('Change Password')
                        ->deliver($message);
                        $this->Flash->success(__('Email Sent'));

                    $code = 200;
                    $status = $this->Response->responseFormat($code);
                }
            } else {
                $code = 404;
                $status = $this->Response->responseFormat($code);
            }
            $this->response = $this->response->withStatus($status['code']);
            $this->set(compact(['status']));
            $this->viewBuilder()->setOption('serialize', ['status']);
        }
    }

    //User's Change Password
    public function changePassword($code = null)
    {
        $status = array();
        $this->request->allowMethod(['patch']);
        $this->Authorization->skipAuthorization();

        $result = $this->Users
            ->findByEmailToken($code)
            ->first();
        if (empty($result)) {
            throw new NotFoundException();
        }
        if ($this->request->is(['patch', 'post', 'put'])) {
            if ($result['verified'] == 0 && $result['email_token'] === $code) {
                $user = $this->Users->patchEntity($result, $this->request->getData());
                $user['verified'] = 1;
                $user['password'] = $this->request->getData('new_password');
                $user['modified'] = FrozenTime::now('Asia/Hong_Kong');
                $user['full_name'] = $user['full_name'];
                $user['username'] = $user['username'];
                $user['email'] = $user['email'];
                if (!$user->getError('new_password')) {
                    if ($this->Users->save($user)) {
                        $code = 200;
                        $status = $this->Response->responseFormat($code);
                    }
                } else {
                    $code = 400;
                    $status = $this->Response->responseFormat($code);
                    $status['error'] = $user->getError('new_password');
                }
            } else {
                $code = 401;
                $status = $this->Response->responseFormat($code);
                $status['message'] = 'Email token already used.';
            }
            $this->response = $this->response->withStatus($status['code']);
            $this->set(compact(['status']));
            $this->viewBuilder()->setOption('serialize', ['status']);
        }
    }

    public function logout()
    {
        $this->Authorization->skipAuthorization();
        $this->request->allowMethod(['post']);
        $result = $this->Authentication->getResult();
        if ($result->isValid()) {
            $this->Authentication->logout();
            $code = 200;
            $status = $this->Response->responseFormat($code);
            $this->response = $this->response->withStatus($status['code']);
            $this->set(compact(['status']));
            $this->viewBuilder()->setOption('serialize', ['status']);
        } else {
            $code = 500;
            $status = $this->Response->responseFormat($code);
            $this->response = $this->response->withStatus($status['code']);
            $this->set(compact(['status']));
            $this->viewBuilder()->setOption('serialize', ['status']);
        }
    }

    //Posts index
    public function index()
    {
        $status = array();
        $this->request->allowMethod(['get']);
        $post = $this->Posts->newEmptyEntity();
        $this->Authorization->authorize($post);
        $this->set(compact('post'));
        $headerAuth = $this->request->getHeaderLine('Authorization');
        $header = $this->JWTTokens->checkHeaderAuth($headerAuth);
        if (isset($header)) {
            $userInfo = $this->JWTTokens->checkToken($header);
        }
        
        $user = $this->Authentication->getIdentity();
        if (!empty($userInfo)) {
            $postsCondition = [
                'OR' => [
                    'Posts.user_id' => $user['id'],
                    [
                        'Follower.follower_user_id' => $user['id'],
                        'Follower.deleted' => 0
                    ]
                ],
                'Posts.deleted' => 0
            ];
            $page_settings = [
                'conditions' => $postsCondition,
                'join' => [
                    [
                        'table' => 'followers',
                        'alias' => 'Follower',
                        'type' => 'LEFT',
                        'conditions' => 'Follower.user_id = Posts.user_id',
                    ],
                ],
                'group' => ['Posts.id'],
                'contain' => ['Users'],
                'limit' => 10,
                'order' => [
                    'Posts.created' => 'desc'
                ]
            ];
            $posts = $this->Paginator->paginate($this->Posts, $page_settings);
            $page = $this->request->getAttribute('paging');

            $likeConditions = [
                'PostLikes.user_id' => $user['id'],
                'PostLikes.deleted' => 0
            ];
            $likes = $this->PostLikes->find(
                'all',
                [
                    'conditions' => $likeConditions,
                    'contain' => 'Users', 'Posts',
                    'order' => ['PostLikes.created' => 'desc'],
                ]
            );

            $code = 200;
            $status = $this->Response->responseFormat($code);
            $this->response = $this->response->withStatus($code);
            $this->set(compact(['status', 'posts', 'likes', 'page_settings']));
            $this->viewBuilder()->setOption('serialize', ['status', 'posts', 'likes', 'page_settings']);
        } else {
            $code = 401;
            $status = $this->Response->responseFormat($code);
            $status['error'] = 'Authorization token is not valid.';
            $this->response = $this->response->withStatus($code);
            $this->set(compact(['status']));
            $this->viewBuilder()->setOption('serialize', ['status']);
        }
    }

    // Post Add
    public function addPost()
    {
        $status = array();
        $this->request->allowMethod(['post']);
        $post = $this->Posts->newEmptyEntity();
        $this->Authorization->authorize($post);
        $headerAuth = $this->request->getHeaderLine('Authorization');
        $header = $this->JWTTokens->checkHeaderAuth($headerAuth);
        if (isset($header)) {
            $userInfo = $this->JWTTokens->checkToken($header);
        }
        if (!empty($userInfo)) {
            if ($this->request->is('post')) {
                $post = $this->Posts->patchEntity($post, $this->request->getData());
                $postImage = $this->request->getData('image_file');
                if (!$post->getErrors()) {
                    if ($this->Posts->save($post)) {
                        $code = 201;
                        $status = $this->Response->responseFormat($code);
                    }
                } else {
                    $code = 400;
                    $status = $this->Response->responseFormat($code);
                    $status['error'] = $post->getErrors();
                }

                $this->response = $this->response->withStatus($status['code']);
                $this->set(compact(['status', 'post']));
                $this->viewBuilder()->setOption('serialize', ['status', 'post']);
            }
        } else {
            $code = 401;
            $status = $this->Response->responseFormat($code);
            $status['error'] = 'Authorization token is not valid.';
            $this->response = $this->response->withStatus($code);
            $this->set(compact(['status']));
            $this->viewBuilder()->setOption('serialize', ['status']);
        }
    }

    //Post Edit
    public function editPost($id = null)
    {
        $this->request->allowMethod(['put']);
        $headerAuth = $this->request->getHeaderLine('Authorization');
        $header = $this->JWTTokens->checkHeaderAuth($headerAuth);
        $user = $this->Users->findByAccessToken($header)->first();
        $result = $this->Posts->findById($id)
            ->first();
        if (empty($result)) {
            throw new ConflictException(__('Post not found.'));
        } elseif ($user['id'] != $result['user_id']) {
            throw new UnauthorizedException(__('You are not authorized to this post.'));
        }
        $post = $this->Posts->get($id);
        $this->Authorization->authorize($post);
        if (isset($header)) {
            $userInfo = $this->JWTTokens->checkToken($header);
        }
        if (!empty($userInfo)) {
            if ($post['deleted'] == 0) {
                if ($this->request->is(['put'])) {
                    $post = $this->Posts->patchEntity($post, $this->request->getData());
                    if (!$post->getErrors()) {
                        if ($this->Posts->save($post)) {
                            $code = 200;
                            $status = $this->Response->responseFormat($code);
                        } else {
                            $code = 401;
                            $status = $this->Response->responseFormat($code);
                        }
                    } else {
                        $code = 400;
                        $status = $this->Response->responseFormat($code);
                        $status['error'] = $post->getErrors();
                    }
                    $this->response = $this->response->withStatus($status['code']);
                    $this->set(compact(['status', 'post']));
                    $this->viewBuilder()->setOption('serialize', ['status', 'post']);
                }
            } else {
                $code = 400;
                $status = $this->Response->responseFormat($code);
                $this->response = $this->response->withStatus($status['code']);
                $this->set(compact(['status']));
                $this->viewBuilder()->setOption('serialize', ['status']);
            }
        } else {
            $code = 401;
            $status = $this->Response->responseFormat($code);
            $status['error'] = 'Authorization token is not valid.';
            $this->set(compact(['status']));
            $this->viewBuilder()->setOption('serialize', ['status']);
        }
    }

    // Post Delete
    public function deletePost($id = null)
    {
        $status = array();
        $this->request->allowMethod(['delete']);
        $headerAuth = $this->request->getHeaderLine('Authorization');
        $header = $this->JWTTokens->checkHeaderAuth($headerAuth);
        $user = $this->Users->findByAccessToken($header)->first();
        $result = $this->Posts->findById($id)
            ->first();
        if (empty($result)) {
            throw new NotFoundException(__('Record not found.'));
        } elseif ($user['id'] != $result['user_id']) {
            throw new UnauthorizedException(__('Unauthorized.'));
        }
        
        $post = $this->Posts->get($id);
        $this->Authorization->authorize($post);
        if (isset($header)) {
            $userInfo = $this->JWTTokens->checkToken($header);
        }
        if (!empty($userInfo)) {
            $post['deleted'] = 1;
            $post['deleted_date'] = FrozenTime::now('Asia/Hong_Kong');
            if ($this->Posts->save($post)) {
                $code = 200;
                $status = $this->Response->responseFormat($code);
            } else {
                $code = 401;
                $status = $this->Response->responseFormat($code);
            }
            $this->response = $this->response->withStatus($status['code']);
            $this->set(compact(['status', 'post']));
            $this->viewBuilder()->setOption('serialize', ['status', 'post']);
        } else {
            $code = 401;
            $status = $this->Response->responseFormat($code);
            $status['error'] = 'Authorization token is not valid.';
            $this->response = $this->response->withStatus($code);
            $this->set(compact(['status']));
            $this->viewBuilder()->setOption('serialize', ['status']);
        }
    }

    // Search
    public function search()
    {
        $this->request->allowMethod(['get']);
        $post = $this->Posts->newEmptyEntity();
        $this->Authorization->authorize($post);
        $user = $this->Authentication->getIdentity();
        $headerAuth = $this->request->getHeaderLine('Authorization');
        $header = $this->JWTTokens->checkHeaderAuth($headerAuth);
        if (isset($header)) {
            $userInfo = $this->JWTTokens->checkToken($header);
        }
        if (!empty($userInfo)) {
            if (!empty($this->request->getQuery('keyword'))) {
                $keyword = $this->request->getQuery('keyword');
                $conditions = [
                    'OR' => [
                        'Users.full_name LIKE' => '%' . $keyword . '%',
                        'Users.username LIKE' => '%' . $keyword . '%',
                        'Posts.post LIKE' => '%' . $keyword . '%'
                    ],
                    'Users.verified' => 1,
                    'Posts.deleted' => 0
                ];
            } else {
                $conditions = '';
            }

            $page_settings = [
                'conditions' => $conditions,
                'contain' => ['Users'],
                'limit' => 5,
                'order' => [
                    'Posts.created' => 'desc'
                ]
            ];
            $results = $this->Paginator->paginate($this->Posts, $page_settings);
            $page = $this->request->getAttribute('paging');

            $likeConditions = [
                'PostLikes.user_id' => $user['id'],
                'PostLikes.deleted' => 0
            ];
            $likes = $this->PostLikes->find(
                'all',
                [
                    'conditions' => $likeConditions,
                    'contain' => 'Users', 'Posts',
                    'order' => ['PostLikes.created' => 'desc'],
                ]
            );

            if (!empty($conditions)) {
                $code = 200;
                $status = $this->Response->responseFormat($code);
                $this->response = $this->response->withStatus($status['code']);
                $this->set(compact(['status', 'results', 'likes', 'page_settings']));
                $this->viewBuilder()->setOption('serialize', ['status', 'results', 'likes', 'page_settings']);
            } else {
                $code = 400;
                $status = $this->Response->responseFormat($code);
                $this->response = $this->response->withStatus($status['code']);
                $this->set(compact(['status']));
                $this->viewBuilder()->setOption('serialize', ['status']);
            }
        } else {
            $code = 401;
            $status = $this->Response->responseFormat($code);
            $status['error'] = 'Authorization token is not valid.';
            $this->response = $this->response->withStatus($code);
            $this->set(compact(['status']));
            $this->viewBuilder()->setOption('serialize', ['status']);
        }
    }

    // Comment Add
    public function addComment($id = null)
    {
        $status = array();
        $this->request->allowMethod(['post', 'get']);
        $comment = $this->Comments->newEmptyEntity();
        $this->Authorization->authorize($comment);
        $headerAuth = $this->request->getHeaderLine('Authorization');
        $header = $this->JWTTokens->checkHeaderAuth($headerAuth);
        $userLogged = $this->Users->findByAccessToken($header)->first();
        $result = $this->Posts->findById($id)
            ->first();
        if (empty($result)) {
            throw new NotFoundException(__('Post not found.'));
        }
        if (isset($header)) {
            $userInfo = $this->JWTTokens->checkToken($header);
        }
        if (!empty($userInfo)) {
            $user = $this->Authentication->getIdentity();
            $post = $this->Posts->get($id, [
                'contain' => ['Users']
            ]);
            $conditions = [
                'Comments.deleted' => 0,
                'Comments.post_id' => $post['id']
            ];
            $this->set(compact('post', [
                'conditions' => $conditions
            ]));
            $commentQuery = $this->Comments->find(
                'all',
                array(
                    'conditions' => $conditions,
                    'contain' => ['Users', 'Posts']
                )
            );
            $commentCount = $commentQuery->count();

            $page_settings = [
                'conditions' => $conditions,
                'contain' => ['Users', 'Posts'],
                'limit' => 5,
                'order' => [
                    'created' => 'desc'
                ]
            ];
            $post_comments = $this->Paginator->paginate($this->Comments, $page_settings);
            $page = $this->request->getAttribute('paging');

            if ($this->request->is('post')) {
                $comment = $this->Comments->patchEntity($comment, $this->request->getData());
                $comment['post_id'] = $post['id'];
                $comment['user_id'] = $user['id'];
                if (!$comment->getErrors()) {
                    if ($this->Comments->save($comment)) {
                        $code = 201;
                        $status = $this->Response->responseFormat($code);
                    }
                } else {
                    $code = 400;
                    $status = $this->Response->responseFormat($code);
                    $status['error'] = $comment->getErrors();
                }
                $this->response = $this->response->withStatus($status['code']);
                $this->set(compact([
                    'status', 'post', 'comment_count', 'post_comments', 'page_settings'
                ]));
                $this->viewBuilder()->setOption('serialize', [
                    'status', 'post', 'comment_count', 'post_comments', 'page_settings'
                ]);
            } else {
                $this->set(compact(['post', 'comment_count', 'post_comments', 'page_settings']));
                $this->viewBuilder()->setOption('serialize', [
                    'post', 'comment_count', 'post_comments', 'page_settings'
                ]);
            }
        } else {
            $code = 401;
            $status = $this->Response->responseFormat($code);
            $status['error'] = 'Authorization token is not valid.';
            $this->response = $this->response->withStatus($code);
            $this->set(compact(['status']));
            $this->viewBuilder()->setOption('serialize', ['status']);
        }
    }

    // Comment Edit
    public function editComment($id = null)
    {
        $this->request->allowMethod(['put']);
        $headerAuth = $this->request->getHeaderLine('Authorization');
        $header = $this->JWTTokens->checkHeaderAuth($headerAuth);
        $user = $this->Users->findByAccessToken($header)->first();
        $result = $this->Comments->findById($id)
            ->first();
        if (empty($result)) {
            throw new NotFoundException(__('Comment not found.'));
        } elseif ($user['id'] != $result['user_id']) {
            throw new UnauthorizedException(__('Unauthorized.'));
        }
        $comment = $this->Comments->get($id);
        $this->Authorization->authorize($comment);
        if (isset($header)) {
            $userInfo = $this->JWTTokens->checkToken($header);
        }
        if (!empty($userInfo)) {
            if ($this->request->is(['put'])) {
                $comment = $this->Comments->patchEntity($comment, $this->request->getData());
                if (!$comment->getErrors()) {
                    if ($this->Comments->save($comment)) {
                        $code = 200;
                        $status = $this->Response->responseFormat($code);
                    }
                } else {
                    $code = 401;
                    $status = $this->Response->responseFormat($code);
                    $status['error'] = $comment->getErrors();
                }
                $this->response = $this->response->withStatus($status['code']);
                $this->set(compact(['status', 'comment']));
                $this->viewBuilder()->setOption('serialize', ['status', 'comment']);
            }
        } else {
            $code = 401;
            $status = $this->Response->responseFormat($code);
            $status['error'] = 'Authorization token is not valid.';
            $this->response = $this->response->withStatus($code);
            $this->set(compact(['status']));
            $this->viewBuilder()->setOption('serialize', ['status']);
        }
    }

    // Comment Delete
    public function deleteComment($id = null)
    {
        $this->request->allowMethod(['delete']);
        $headerAuth = $this->request->getHeaderLine('Authorization');
        $header = $this->JWTTokens->checkHeaderAuth($headerAuth);
        $user = $this->Users->findByAccessToken($header)->first();
        $result = $this->Comments->findById($id)
            ->first();
        if (empty($result)) {
            throw new NotFoundException(__('Record not found.'));
        } elseif ($user['id'] != $result['user_id']) {
            throw new UnauthorizedException();
        }
        $comment = $this->Comments->get($id);
        $this->Authorization->authorize($comment);
        if (isset($header)) {
            $userInfo = $this->JWTTokens->checkToken($header);
        }
        if (!empty($userInfo)) {
            $comment['deleted'] = 1;
            if ($this->Comments->save($comment)) {
                $code = 200;
                $status = $this->Response->responseFormat($code);
            } else {
                $code = 401;
                $status = $this->Response->responseFormat($code);
            }
            $this->response = $this->response->withStatus($status['code']);
            $this->set(compact(['status', 'comment']));
            $this->viewBuilder()->setOption('serialize', ['status', 'comment']);
        } else {
            $code = 401;
            $status = $this->Response->responseFormat($code);
            $status['error'] = 'Authorization token is not valid.';
            $this->response = $this->response->withStatus($code);
            $this->set(compact(['status']));
            $this->viewBuilder()->setOption('serialize', ['status']);
        }
    }

    // Like Post
    public function like($id)
    {
        $this->request->allowMethod(['post']);
        $headerAuth = $this->request->getHeaderLine('Authorization');
        $header = $this->JWTTokens->checkHeaderAuth($headerAuth);
        $result = $this->Posts->findById($id)
            ->first();
        if (empty($result)) {
            throw new NotFoundException();
        }
        $post = $this->Posts->get($id);
        $user = $this->Authentication->getIdentity();
        $userAuthorized = $this->Users->get($user['id']);
        $this->Authorization->authorize($userAuthorized);
        if (isset($header)) {
            $userInfo = $this->JWTTokens->checkToken($header);
        }
        if (!empty($userInfo)) {
            if ($post != null) {
                $conditions = [
                    'PostLikes.user_id' => $user['id'],
                    'PostLikes.post_id' => $post['id']
                ];
                $likePost = $this->PostLikes->find('all', [
                    'conditions' => $conditions
                ]);
                $like = $likePost->first();

                if (!empty($like)) {
                    if ($like['deleted'] == 1) {
                        $like['deleted'] = 0;
                        $like['modified'] = FrozenTime::now('Asia/Hong_Kong');
                        $this->PostLikes->save($like);
                        $code = 200;
                        $status = $this->Response->responseFormat($code);
                    } else {
                        $code = 403;
                        $status = $this->Response->responseFormat($code);
                    }
                    $this->response = $this->response->withStatus($status['code']);
                    $this->set(compact(['status', 'like']));
                    $this->viewBuilder()->setOption('serialize', ['status', 'like']);
                } else {
                    $post_like = $this->PostLikes->newEmptyEntity();
                    $post_like['post_id'] = $post['id'];
                    $post_like['user_id'] = $user['id'];
                    if ($this->PostLikes->save($post_like)) {
                        $code = 200;
                        $status = $this->Response->responseFormat($code);
                    } else {
                        $code = 403;
                        $status = $this->Response->responseFormat($code);
                    }
                    $this->response = $this->response->withStatus($status['code']);
                    $this->set(compact(['status', 'post_like']));
                    $this->viewBuilder()->setOption('serialize', ['status', 'post_like']);
                }
            }
        } else {
            $code = 401;
            $status = $this->Response->responseFormat($code);
            $status['error'] = 'Authorization token is not valid.';
            $this->response = $this->response->withStatus($code);
            $this->set(compact(['status']));
            $this->viewBuilder()->setOption('serialize', ['status']);
        }
    }

    // Dislike Post
    public function dislike($id)
    {
        $this->request->allowMethod(['delete']);
        $headerAuth = $this->request->getHeaderLine('Authorization');
        $header = $this->JWTTokens->checkHeaderAuth($headerAuth);
        $result = $this->PostLikes->findById($id)
            ->first();
        if (empty($result)) {
            throw new NotFoundException();
        }
        $post = $this->Posts->get($id);
        $user = $this->Authentication->getIdentity();
        $userAuthorized = $this->Users->get($user['id']);
        $this->Authorization->authorize($userAuthorized);
        if (isset($header)) {
            $userInfo = $this->JWTTokens->checkToken($header);
        }
        if (!empty($userInfo)) {
            if ($post != null) {
                $conditions = array(
                    'PostLikes.post_id' => $post['id'],
                    'PostLikes.user_id' => $user['id']
                );

                $dislikePost = $this->PostLikes->find('all', [
                    'conditions' => $conditions
                ]);
                $dislike = $dislikePost->first();

                if (!empty($dislike)) {
                    if ($dislike['deleted'] == 0) {
                        $dislike['deleted'] = 1;
                        $dislike['modified'] = FrozenTime::now('Asia/Hong_Kong');
                        $this->PostLikes->save($dislike);
                        $code = 200;
                        $status = $this->Response->responseFormat($code);
                    } else {
                        $code = 403;
                        $status = $this->Response->responseFormat($code);
                    }
                    $this->response = $this->response->withStatus($status['code']);
                    $this->set(compact(['status', 'dislike']));
                    $this->viewBuilder()->setOption('serialize', ['status', 'dislike']);
                } else {
                    $code = 404;
                    $status = $this->Response->responseFormat($code);
                    $this->response = $this->response->withStatus($status['code']);
                    $this->set(compact(['status']));
                    $this->viewBuilder()->setOption('serialize', ['status']);
                }
            }
        } else {
            $code = 401;
            $status = $this->Response->responseFormat($code);
            $status['error'] = 'Authorization token is not valid.';
            $this->response = $this->response->withStatus($code);
            $this->set(compact(['status']));
            $this->viewBuilder()->setOption('serialize', ['status']);
        }
    }

    // Follow User
    public function follow($id)
    {
        $status = array();
        $this->request->allowMethod(['post']);
        $headerAuth = $this->request->getHeaderLine('Authorization');
        $header = $this->JWTTokens->checkHeaderAuth($headerAuth);
        $result = $this->Users->findById($id)
            ->first();
        if (empty($result)) {
            throw new NotFoundException();
        }
        $following = $this->Users->get($id);
        $this->Authorization->authorize($following);
        $user = $this->Authentication->getIdentity();
        if (isset($header)) {
            $userInfo = $this->JWTTokens->checkToken($header);
        }
        if (!empty($userInfo)) {
            if ((!empty($following)) && ($following['verified'] == 1)) {
                $conditions = [
                    'Followers.user_id' => $following['id'],
                    'Followers.follower_user_id' => $user['id']
                ];
                $followUser = $this->Followers->find('all', [
                    'conditions' => $conditions
                ]);
                $follow = $followUser->first();

                if (!empty($follow)) {
                    if ($follow['deleted'] == 1) {
                        $follow['deleted'] = 0;
                        $follow['modified'] = FrozenTime::now('Asia/Hong_Kong');
                        $this->Followers->save($follow);
                        $code = 200;
                        $status = $this->Response->responseFormat($code);
                    } else {
                        $code = 403;
                        $status = $this->Response->responseFormat($code);
                    }
                    $this->response = $this->response->withStatus($status['code']);
                    $this->set(compact(['status', 'follow']));
                    $this->viewBuilder()->setOption('serialize', ['status', 'follow']);
                } else {
                    $new_following = $this->Followers->newEmptyEntity();
                    $new_following['user_id'] = $following['id'];
                    $new_following['follower_user_id'] = $user['id'];
                    if ($this->Followers->save($new_following)) {
                        $code = 201;
                        $status = $this->Response->responseFormat($code);
                    } else {
                        $code = 403;
                        $status = $this->Response->responseFormat($code);
                    }
                    $this->response = $this->response->withStatus($status['code']);
                    $this->set(compact(['status', 'new_following']));
                    $this->viewBuilder()->setOption('serialize', ['status', 'new_following']);
                }
            } else {
                $code = 404;
                $status = $this->Response->responseFormat($code);
                $this->response = $this->response->withStatus($status['code']);
                $this->set(compact(['status']));
                $this->viewBuilder()->setOption('serialize', ['status']);
            }
        } else {
            $code = 401;
            $status = $this->Response->responseFormat($code);
            $status['error'] = 'Authorization token is not valid.';
            $this->response = $this->response->withStatus($code);
            $this->set(compact(['status']));
            $this->viewBuilder()->setOption('serialize', ['status']);
        }
    }

    // Unfollow User
    public function unfollow($id)
    {
        $status = array();
        $this->request->allowMethod(['delete']);
        $headerAuth = $this->request->getHeaderLine('Authorization');
        $header = $this->JWTTokens->checkHeaderAuth($headerAuth);
        $result = $this->Users->findById($id)
            ->first();
        if (empty($result)) {
            throw new NotFoundException();
        }
        $following = $this->Users->get($id);
        $this->Authorization->authorize($following);
        $user = $this->Authentication->getIdentity();
        if (isset($header)) {
            $userInfo = $this->JWTTokens->checkToken($header);
        }
        if (!empty($userInfo)) {
            if ($following != null) {
                $conditions = [
                    'Followers.user_id' => $following['id'],
                    'Followers.follower_user_id' => $user['id']
                ];
                $unfollowUser = $this->Followers->find('all', [
                    'conditions' => $conditions
                ]);
                $unfollow = $unfollowUser->first();

                if (!empty($unfollow)) {
                    if ($unfollow['deleted'] == 0) {
                        $unfollow['deleted'] = 1;
                        $unfollow['modified'] = FrozenTime::now('Asia/Hong_Kong');
                        $this->Followers->save($unfollow);
                        $code = 200;
                        $status = $this->Response->responseFormat($code);
                    } else {
                        $code = 403;
                        $status = $this->Response->responseFormat($code);
                    }
                    $this->response = $this->response->withStatus($status['code']);
                    $this->set(compact(['status', 'unfollow']));
                    $this->viewBuilder()->setOption('serialize', ['status', 'unfollow']);
                } else {
                    $code = 404;
                    $status = $this->Response->responseFormat($code);
                    $this->response = $this->response->withStatus($status['code']);
                    $this->set(compact(['status']));
                    $this->viewBuilder()->setOption('serialize', ['status']);
                }
            }
        } else {
            $code = 401;
            $status = $this->Response->responseFormat($code);
            $status['error'] = 'Authorization token is not valid.';
            $this->response = $this->response->withStatus($code);
            $this->set(compact(['status']));
            $this->viewBuilder()->setOption('serialize', ['status']);
        }
    }

    // Followers View
    public function viewFollowers()
    {
        $status = array();
        $this->request->allowMethod(['get']);
        $user = $this->Authentication->getIdentity();
        $userAuthorized = $this->Users->get($user['id']);
        $this->Authorization->authorize($userAuthorized);
        $headerAuth = $this->request->getHeaderLine('Authorization');
        $header = $this->JWTTokens->checkHeaderAuth($headerAuth);
        if (isset($header)) {
            $userInfo = $this->JWTTokens->checkToken($header);
        }
        if (!empty($userInfo)) {
            $following = [
                'Followers.user_id' => $user['id'],
                'Followers.deleted' => 0
            ];
            $page_settings = [
                'conditions' => $following,
                'contain' => ['UsersFollowing', 'UsersFollowers'],
            ];
            $user_followers = $this->Paginator->paginate($this->Followers, $page_settings);
            $page = $this->request->getAttribute('paging');

            $code = 200;
            $status = $this->Response->responseFormat($code);
            $this->response = $this->response->withStatus($status['code']);
            $this->set(compact(['status', 'user_followers', 'page_settings']));
            $this->viewBuilder()->setOption('serialize', ['status', 'user_followers', 'page_settings']);
        } else {
            $code = 401;
            $status = $this->Response->responseFormat($code);
            $status['error'] = 'Authorization token is not valid.';
            $this->response = $this->response->withStatus($code);
            $this->set(compact(['status']));
            $this->viewBuilder()->setOption('serialize', ['status']);
        }
    }

    // Following View
    public function viewFollowing()
    {
        $status = array();
        $this->request->allowMethod(['get']);
        $user = $this->Authentication->getIdentity();
        $userAuthorized = $this->Users->get($user['id']);
        $this->Authorization->authorize($userAuthorized);
        $headerAuth = $this->request->getHeaderLine('Authorization');
        $header = $this->JWTTokens->checkHeaderAuth($headerAuth);
        if (isset($header)) {
            $userInfo = $this->JWTTokens->checkToken($header);
        }
        if (!empty($userInfo)) {
            $following = [
                'Followers.follower_user_id' => $user['id'],
                'Followers.deleted' => 0
            ];
            $page_settings = [
                'conditions' => $following,
                'contain' => ['UsersFollowing', 'UsersFollowers'],
                'limit' => 5,
                'order' => [
                    'Followers.modified' => 'desc'
                ]
            ];
            $user_following = $this->Paginator->paginate($this->Followers, $page_settings);
            $page = $this->request->getAttribute('paging');

            $code = 200;
            $status = $this->Response->responseFormat($code);
            $this->response = $this->response->withStatus($status['code']);
            $this->set(compact(['status', 'user_following', 'page_settings']));
            $this->viewBuilder()->setOption('serialize', ['status', 'user_following', 'page_settings']);
        } else {
            $code = 401;
            $status = $this->Response->responseFormat($code);
            $status['error'] = 'Authorization token is not valid.';
            $this->response = $this->response->withStatus($code);
            $this->set(compact(['status']));
            $this->viewBuilder()->setOption('serialize', ['status']);
        }
    }

    // Retweet Post
    public function retweet($id)
    {
        $this->request->allowMethod(['post']);
        $result = $this->Posts->findById($id)
            ->first();
        if (empty($result)) {
            throw new NotFoundException();
        }
        $user = $this->Authentication->getIdentity();
        $post = $this->Posts->get($id);
        $this->Authorization->authorize($post);
        $headerAuth = $this->request->getHeaderLine('Authorization');
        $header = $this->JWTTokens->checkHeaderAuth($headerAuth);
        if (isset($header)) {
            $userInfo = $this->JWTTokens->checkToken($header);
        }
        if (!empty($userInfo)) {
            $conditions = [
                'Retweets.user_id' => $user['id'],
                'Retweets.post_id' => $post['id']
            ];
            $retweetPost = $this->Retweets->find('all');
            $retweet = $retweetPost->first();

            if ($retweet['deleted'] == 1) {
                $retweet['deleted'] = 0;
                $retweet['modified'] = FrozenTime::now('Asia/Hong_Kong');
                if ($this->Retweets->save($retweet)) {
                    $code = 201;
                    $status = $this->Response->responseFormat($code);
                } else {
                    $code = 403;
                    $status = $this->Response->responseFormat($code);
                }
                $this->response = $this->response->withStatus($status['code']);
                $this->set(compact(['status', 'retweet']));
                $this->viewBuilder()->setOption('serialize', ['status', 'retweet']);
            } else {
                $post_retweet = $this->Retweets->newEmptyEntity();
                $post_retweet['post_id'] = $post['id'];
                $post_retweet['user_id'] = $user['id'];
                if ($this->Retweets->save($post_retweet)) {
                    $code = 201;
                    $status = $this->Response->responseFormat($code);
                } else {
                    $code = 403;
                    $status = $this->Response->responseFormat($code);
                }
                $this->response = $this->response->withStatus($status['code']);
                $this->set(compact(['status', 'post_retweet']));
                $this->viewBuilder()->setOption('serialize', ['status', 'post_retweet']);
            }
        } else {
            $code = 401;
            $status = $this->Response->responseFormat($code);
            $status['error'] = 'Authorization token is not valid.';
            $this->response = $this->response->withStatus($code);
            $this->set(compact(['status']));
            $this->viewBuilder()->setOption('serialize', ['status']);
        }
    }

    // Retweet Delete
    public function deleteRetweet($id = null)
    {
        $this->request->allowMethod(['delete']);
        $result = $this->Retweets->findById($id)
            ->first();
        if (empty($result)) {
            throw new NotFoundException();
        }
        $retweet = $this->Retweets->get($id);
        $this->Authorization->authorize($retweet);
        $headerAuth = $this->request->getHeaderLine('Authorization');
        $header = $this->JWTTokens->checkHeaderAuth($headerAuth);
        if (isset($header)) {
            $userInfo = $this->JWTTokens->checkToken($header);
        }
        if (!empty($userInfo)) {
            if ($retweet['deleted'] == 0) {
                $retweet['deleted'] = 1;
                if ($this->Retweets->save($retweet)) {
                    $code = 200;
                    $status = $this->Response->responseFormat($code);
                } else {
                    $code = 401;
                    $status = $this->Response->responseFormat($code);
                }
            } else {
                $code = 404;
                $status = $this->Response->responseFormat($code);
            }
            $this->response = $this->response->withStatus($status['code']);
            $this->set(compact(['status']));
            $this->viewBuilder()->setOption('serialize', ['status']);
        } else {
            $code = 401;
            $status = $this->Response->responseFormat($code);
            $status['error'] = 'Authorization token is not valid.';
            $this->response = $this->response->withStatus($code);
            $this->set(compact(['status']));
            $this->viewBuilder()->setOption('serialize', ['status']);
        }
    }

    public function unauthenticated()
    {
        $this->Authorization->skipAuthorization();
        $header = $this->request->getHeaderLine('Authorization');
        $result = $this->Authentication->getResult();
        if ($result->isValid()) {
            if (!empty($header)) {
                $userInfo = $this->JWTTokens->checkToken($header);
                if (empty($userInfo)) {
                    $error = 'Authorization token is invalid.';
                } else {
                    $error = 'Authorization token already expired.';
                }
            } else {
                throw new UnauthorizedException();
            }
        } else {
            $error = 'You need to login to access this application.';
        }
        $code = 401;
        $status = $this->Response->responseFormat($code);
        $status['error'] = $error;
        $this->response = $this->response->withStatus($code);
        $this->set(compact(['status']));
        $this->viewBuilder()->setOption('serialize', ['status']);
        
    }
}
