<?php

declare(strict_types=1);

namespace App\Controller;

use Cake\I18n\FrozenTime;
use App\Controller\AppController;
use Cake\Http\Client;

/**
 * Posts Controller
 *
 * @property \App\Model\Table\PostsTable $Posts
 * @method \App\Model\Entity\Post[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class PostsController extends AppController
{
    /**
     * Index method
     *
     * @return \Cake\Http\Response|null|void Renders view
     */
    public function index()
    {
        $post = $this->Posts->newEmptyEntity();
        $this->Authorization->authorize($post);
        $url =  "http://" . $_SERVER['HTTP_HOST'] . '/microblog4api/api/index.json';
        $session = $this->request->getSession();
        $token = $session->read('token');
        $http = new Client([
            'headers' => ['Authorization' => $token],
        ]);
        $response = $http->get($url);
        $post = json_decode($response->getStringBody(), true);

        if ($post['status']['code'] == 200) {
            $post['posts'] = $this->Paginator->paginate($this->Posts, $post['page_settings']);
            $posts = $post['posts'];
            $likes = $post['likes'];
            $this->set(compact(['posts', 'likes']));
        }

        

        $this->set('title', 'Microblog 4 - Home Page');

        return $this->userInfo();
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $post = $this->Posts->newEmptyEntity();
        $this->Authorization->authorize($post);
        if ($this->request->is('post')) {
            $post = $this->Posts->patchEntity($post, $this->request->getData());
            $url =  "http://" . $_SERVER['HTTP_HOST'] . '/microblog4api/api/addPost.json';
            $user = $this->Authentication->getIdentity();
            $session = $this->request->getSession();
            $token = $session->read('token');
            $http = new Client([
                'headers' => ['Authorization' => $token],
            ]);
            if ($this->request->getData('image_file')->getClientFilename() != '') {
                $data = [
                    'post' => $this->request->getData('post'),
                    'image_file' => $this->request->getData('image_file')->getClientFilename(),
                    'image' => $this->request->getData('image_file')->getClientFilename(),
                    'user_id' => $user['id']
                ];
            } else {
                $data = [
                    'post' => $this->request->getData('post'),
                    'user_id' => $user['id']
                ];
            }
            $response = $http->post($url, $data);
            $addPost = json_decode($response->getStringBody(), true);

            if ($addPost['status']['code'] == 201) {
                if (!empty($data['image'])) {
                    $targetPath = WWW_ROOT . 'img' . DS . 'postImages' . DS . $data['image'];
                    $this->request->getData('image_file')->moveTo($targetPath);
                }
                $this->Flash->success(__('The post has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The post could not be saved. Please, try again.'));
            }
        }
        $this->set(compact('post'));

        $this->set('title', 'Microblog 4 - Add Post');
        return $this->userInfo();
    }

    public function edit($id = null)
    {
        $user = $this->Authentication->getIdentity();
        $post = $this->Posts->get($id);
        if (empty($id)) {
            throw new NotFoundException();
        } elseif ($user['id'] != $post['user_id']) {
            $this->Flash->error(__('You are not authorized to this page.'));
        }
        $this->Authorization->authorize($post);
        if ($post['deleted'] == 0) {
            if ($this->request->is(['patch', 'post', 'put'])) {
                $post = $this->Posts->patchEntity($post, $this->request->getData());
                $url =  "http://" . $_SERVER['HTTP_HOST'] . '/microblog4api/api/editPost/' . $id . '.json';
                
                $session = $this->request->getSession();
                $token = $session->read('token');
                $http = new Client([
                    'headers' => ['Authorization' => $token],
                ]);
                if ($this->request->getData('image_file')->getClientFilename() != '') {
                    $data = [
                        'post' => $this->request->getData('post'),
                        'image_file' => $this->request->getData('image_file')->getClientFilename(),
                        'image' => $this->request->getData('image_file')->getClientFilename(),
                        'user_id' => $user['id']
                    ];
                } else {
                    $data = [
                        'post' => $this->request->getData('post'),
                        'user_id' => $user['id']
                    ];
                }

                $response = $http->put($url, $data);
                $editPost = json_decode($response->getStringBody(), true);

                if ($editPost['status']['code'] == 200) {
                    if (!empty($data['image'])) {
                        $targetPath = WWW_ROOT . 'img' . DS . 'postImages' . DS . $data['image'];
                        $this->request->getData('image_file')->moveTo($targetPath);
                    }
                    $this->Flash->success(__('The post has been updated.'));
                    return $this->redirect(['action' => 'index']);
                } else {
                    $this->Flash->error(__('The post could not be updated. Please, try again.'));
                }
            }
        } else {
            $this->Flash->error(__('Data does not exist.'));
            return $this->redirect(['action' => 'index']);
        }
        $this->set(compact('post'));

        $this->set('title', 'Microblog 4 - Edit Post');

        return $this->userInfo();
    }

    /**
     * Delete method
     *
     * @param string|null $id Post id.
     * @return \Cake\Http\Response|null|void Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $user = $this->Authentication->getIdentity();
        $post = $this->Posts->get($id);
        if (empty($id)) {
            throw new NotFoundException();
        } elseif ($user['id'] != $post['user_id']) {
            $this->Flash->error(__('You are not authorized to this page.'));
        }
        $this->Authorization->authorize($post);
        if ($post['deleted'] == 0) {
            $url =  "http://" . $_SERVER['HTTP_HOST'] . '/microblog4api/api/deletePost/' . $id . '.json';
            $session = $this->request->getSession();
            $token = $session->read('token');
            $http = new Client([
                'headers' => ['Authorization' => $token],
            ]);

            $response = $http->delete($url);
            $deletePost = json_decode($response->getStringBody(), true);
            if ($deletePost['status']['code'] == 200) {
                $this->Flash->success(__('The post has been deleted.'));
            } else {
                $this->Flash->error(__('The post could not be deleted. Please, try again.'));
            }
        }

        return $this->redirect(['action' => 'index']);
    }

    public function search()
    {
        $post = $this->Posts->newEmptyEntity();
        $this->Authorization->authorize($post);
        $url =  "http://" . $_SERVER['HTTP_HOST'] . '/microblog4api/api/search.json';
        $session = $this->request->getSession();
        $token = $session->read('token');
        $http = new Client([
            'headers' => ['Authorization' => $token],
        ]);

        $response = $http->get($url, [
            'keyword' => $this->request->getQuery('keyword')
        ]);
        $search = json_decode($response->getStringBody(), true);

        if ($search['status']['code'] == 200) {
            $search['results'] = $this->Paginator->paginate($this->Posts, $search['pageSettings']);
            $results = $search['results'];
            $likes = $search['likes'];
            $this->set(compact('results', 'likes'));
        } else {
            $this->Flash->error(__('Please input any keyword.'));
            return $this->redirect(['action' => 'index']);
        }

        $this->set('title', 'Microblog 4 - Search Results');

        return $this->userInfo();
    }
}
