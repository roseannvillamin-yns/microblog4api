<?php

declare(strict_types=1);

namespace App\Controller;

use Cake\I18n\FrozenTime;
use Cake\Http\Client;

class RetweetsController extends AppController
{
    public function retweet($id)
    {
        $post = $this->Posts->get($id);
        $this->Authorization->authorize($post);
        $url =  "http://" . $_SERVER['HTTP_HOST'] . '/microblog4api/api/retweet/' . $id . '.json';
        $session = $this->request->getSession();
        $token = $session->read('token');
        $http = new Client([
            'headers' => ['Authorization' => $token],
        ]);

        $response = $http->post($url);
        $retweetPost = json_decode($response->getStringBody(), true);
        if ($retweetPost['status']['code'] == 200) {
            $this->Flash->success(__('Retweeted'));
        } else {
            $this->Flash->error(__('Failed to retweet.'));
        }
        return $this->redirect(['controller' => 'Posts', 'action' => 'index']);
    }

    /**
     * Delete method
     *
     */
    public function delete($id = null)
    {
        $retweet = $this->Retweets->get($id);
        $this->Authorization->authorize($retweet);
        $url =  "http://" . $_SERVER['HTTP_HOST'] . '/microblog4api/api/deleteRetweet/' . $id . '.json';
        $session = $this->request->getSession();
        $token = $session->read('token');
        $http = new Client([
            'headers' => ['Authorization' => $token],
        ]);

        $response = $http->delete($url);
        $deleteRetweet = json_decode($response->getStringBody(), true);
        if ($deleteRetweet['status']['code'] == 200) {
            $this->Flash->success(__('The retweet has been deleted.'));
        } elseif ($deleteRetweet['status']['code'] == 404) {
            $this->Flash->error(__('Data does not exist.'));
        } else {
            $this->Flash->error(__('The retweet could not be deleted. Please, try again.'));
        }

        return $this->redirect(['controller' => 'Posts', 'action' => 'index']);
    }
}
