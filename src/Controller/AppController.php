<?php

declare(strict_types=1);

namespace App\Controller;

use Cake\Controller\Controller;
use Cake\Http\Client;

/**
 * Application Controller
 *
 * Add your application-wide methods in the class below, your controllers
 * will inherit them.
 *
 * @link https://book.cakephp.org/4/en/controllers.html#the-app-controller
 */
class AppController extends Controller
{
    /**
     * Initialization hook method.
     *
     * Use this method to add common initialization code like loading components.
     *
     * e.g. `$this->loadComponent('FormProtection');`
     *
     * @return void
     */
    public function initialize(): void
    {
        parent::initialize();

        $this->loadComponent('RequestHandler');
        $this->loadComponent('Flash');
        $this->loadComponent('Paginator');
        $this->loadComponent('Authentication', [
            'className' => \App\Controller\Component\AppAuthenticationComponent::class,
        ]);
        $this->loadComponent('Authorization.Authorization');
        $this->loadComponent('JWTTokens');
        $this->loadComponent('Response');

        /*
         * Enable the following component for recommended CakePHP form protection settings.
         * see https://book.cakephp.org/4/en/controllers/components/form-protection.html
         */
        //$this->loadComponent('FormProtection');
    }

    public function beforeFilter(\Cake\Event\EventInterface $event)
    {
        parent::beforeFilter($event);
        $this->loadModel('Users');
        $this->loadModel('Posts');
        $this->loadModel('Retweets');
        $this->loadModel('Followers');
        $this->loadModel('Comments');
        $this->loadModel('PostLikes');
        //$this->Authentication->addUnauthenticatedActions(['index', 'view']);
    }

    public function userInfo()
    {
        $url =  "http://" . $_SERVER['HTTP_HOST'] . '/microblog4api/api/userInfo.json';
        $session = $this->request->getSession();
        $token = $session->read('token');
        $http = new Client([
            'headers' => ['Authorization' => $token],
        ]);
        $response = $http->get($url);
        $userInfo = json_decode($response->getStringBody(), true);

        if ($userInfo['status']['code'] == 200) {
            $retweets = $userInfo['retweet'];
            $following = $userInfo['following'];
            $followers = $userInfo['followers'];
            $followingCount = $userInfo['following_count'];
            $followerCount = $userInfo['follower_count'];
            $likeCount = $userInfo['like_count'];
            $this->set(compact([
                'retweets', 'following', 'followers', 'followingCount', 'followerCount', 'likeCount'
            ]));
        }
    }
}
