<?php

declare(strict_types=1);

namespace App\Controller;

use App\Controller\AppController;
use Cake\Http\Client;

class CommentsController extends AppController
{
    /**
     * Add method
     */
    public function add($id = null)
    {
        $comment = $this->Comments->newEmptyEntity();
        $this->Authorization->authorize($comment);
        $url =  "http://" . $_SERVER['HTTP_HOST'] . '/microblog4api/api/addComment/' . $id . '.json';
        $session = $this->request->getSession();
        $token = $session->read('token');
        $http = new Client([
            'headers' => ['Authorization' => $token],
        ]);

        $response = $http->get($url);
        $comments = json_decode($response->getStringBody(), true);
        $comments['postComments'] = $this->Paginator->paginate($this->Comments, $comments['pageSettings']);

        $post = $comments['post'];
        $commentCount = $comments['commentCount'];
        $postComments = $comments['postComments'];
        $this->set(compact(['post', 'commentCount', 'postComments']));

        if ($this->request->getData('comment') != null) {
            $responsePost = $http->post($url, [
                'comment' => $this->request->getData('comment')
            ]);
            $addComment = json_decode($responsePost->getStringBody(), true);

            if ($addComment['status']['code'] == 201) {
                $this->Flash->success(__('The comment has been saved.'));
                return $this->redirect([
                    'controller' => 'Comments', 'action' => 'add', $addComment['post']['id']
                ]);
            } else {
                $this->Flash->error(__('The comment could not be saved. Please, try again.'));
            }
        }

        $this->set(compact('comment'));

        $this->set('title', 'Microblog 2 - Post Comments');

        return $this->userInfo();
    }

    /**
     * Edit method
     */
    public function edit($id = null)
    {
        if (empty($id)) {
            throw new NotFoundException();
        }
        $comment = $this->Comments->get($id);
        $this->Authorization->authorize($comment);

        if ($comment['deleted'] == 0) {
            if ($this->request->is(['patch', 'post', 'put'])) {
                $url =  "http://" . $_SERVER['HTTP_HOST'] . '/microblog4api/api/editComment/' . $id . '.json';
                $session = $this->request->getSession();
                $token = $session->read('token');
                $http = new Client([
                    'headers' => ['Authorization' => $token],
                ]);

                $response = $http->post($url, [
                    'comment' => $this->request->getData('comment')
                ]);
                $editComment = json_decode($response->getStringBody(), true);

                if ($editComment['status']['code'] == 200) {
                    $this->Flash->success(__('The comment has been updated.'));
                    return $this->redirect([
                        'controller' => 'Comments',
                        'action' => 'add',
                        $editComment['comment']['post_id']
                    ]);
                } else {
                    $this->Flash->error(__('The comment could not be updated. Please, try again.'));
                }
            }
        } else {
            $this->Flash->error(__('Data does not exist.'));
            return $this->redirect(['controller' => 'Posts', 'action' => 'index']);
        }
        $this->set(compact('comment'));

        $this->set('title', 'Microblog 2 - Edit Comment');

        return $this->userInfo();
    }

    /**
     * Delete method
     */
    public function delete($id = null)
    {
        $comment = $this->Comments->get($id);
        $this->Authorization->authorize($comment);
        if ($comment['deleted'] == 0) {
            $url =  "http://" . $_SERVER['HTTP_HOST'] . '/microblog4api/api/deleteComment/' . $id . '.json';
            $session = $this->request->getSession();
            $token = $session->read('token');
            $http = new Client([
                'headers' => ['Authorization' => $token],
            ]);

            $response = $http->post($url);
            $deleteComment = json_decode($response->getStringBody(), true);
            if ($deleteComment['status']['code'] == 200) {
                $this->Flash->success(__('The comment has been deleted.'));
            } else {
                $this->Flash->error(__('The comment could not be deleted. Please, try again.'));
            }
        }

        return $this->redirect(['controller' => 'Comments', 'action' => 'add', $comment['post_id']]);
    }
}
