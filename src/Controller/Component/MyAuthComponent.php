<?php
declare(strict_types=1);

namespace App\Controller\Component;

use Cake\Controller\Component;
use Authentication\PasswordHasher\DefaultPasswordHasher;

class MyAuthComponent extends Component
{
	public function checkUser($password, $userPass)
	{
		if (strlen($password) > 0) {
            return (new DefaultPasswordHasher())->check($password, $userPass);
        }
	}
}