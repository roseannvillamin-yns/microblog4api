<?php
declare(strict_types=1);

namespace App\Controller\Component;

use Cake\Controller\Component;

class ResponseComponent extends Component
{
    public function responseFormat($code)
    {
        if ($code == 200) {
            $status = [
                'type' => 'Success',
                'code' => 200,
                'message' => 'Ok'
            ];
        } elseif ($code == 201) {
            $status = [
                'type' => 'Success',
                'code' => 201,
                'message' => 'Created',
            ];
        } elseif ($code == 400) {
            $status = [
                'type' => 'Failed',
                'code' => 400,
                'message' => 'Bad Request',
            ];
        } elseif ($code == 401) {
            $status = [
                'type' => 'Failed',
                'code' => 401,
                'message' => 'Unauthorized',
            ];
        } elseif ($code == 403) {
            $status = [
                'type' => 'Failed',
                'code' => 403,
                'message' => 'Forbidden',
            ];
        } elseif ($code == 404) {
            $status = [
                'type' => 'Failed',
                'code' => 404,
                'message' => 'Not Found',
            ];
        } elseif ($code == 405) {
            $status = [
                'type' => 'Failed',
                'code' => 405,
                'message' => 'Method Not Allowed',
            ];
        } elseif ($code == 409) {
            $status = [
                'type' => 'Failed',
                'code' => 409,
                'message' => 'User not found',
            ];
        } elseif ($code == 500) {
            $status = [
                'type' => 'Failed',
                'code' => 500,
                'message' => 'Internal Server Error',
            ];
        }

        return $status;
    }
}
