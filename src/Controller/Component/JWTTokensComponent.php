<?php
declare(strict_types=1);

namespace App\Controller\Component;

use Cake\Controller\Component;
use Cake\Controller\ComponentRegistry;
use Cake\Utility\Security;
use Firebase\JWT\JWT;
use App\Model\Table\UsersTable;
use Cake\Http\Exception\UnauthorizedException;

class JWTTokensComponent extends Component
{
    public function generateAccessToken($id)
    {
    	$payload = [
    		'sub' => $id,
    		'exp' => time() + (5 * 60),
    	];
    	$json = [
    		'token' => JWT::encode($payload, Security::getSalt(), 'HS256'),
    	];

        return $json;
    }

    public function checkToken($token)
    {
        if (isset($token)) {
            $user = new UsersTable();
            $user_token = $user->findByAccessToken($token)
                ->first();
        } else {
            $user_token = [];
        }
        

        return $user_token;
    }

    public function checkHeaderAuth($headerAuth)
    {
        if (empty($headerAuth)) {
            throw new UnauthorizedException();
        }
        return $headerAuth;
    }
}
