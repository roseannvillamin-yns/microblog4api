<?php

declare(strict_types=1);

namespace App\Controller;

use Cake\I18n\FrozenTime;
use App\Controller\AppController;
use Cake\Http\Client;

class PostLikesController extends AppController
{
    public function like($id)
    {
        $post = $this->Posts->get($id);
        $user = $this->Authentication->getIdentity();
        if ($post != null) {
            $url =  "http://" . $_SERVER['HTTP_HOST'] . '/microblog4api/api/like/' . $id . '.json';
            $session = $this->request->getSession();
            $token = $session->read('token');
            $http = new Client([
                'headers' => ['Authorization' => $token],
            ]);

            $response = $http->post($url);
            $likePost = json_decode($response->getStringBody(), true);

            if ($likePost['status']['code'] == 200) {
                $this->Flash->success(__('Liked'));
            } else {
                $this->Flash->error(__('You already liked this post.'));
            }
        }

        $userAuthorized = $this->Users->get($user['id']);
        $this->Authorization->authorize($userAuthorized);

        return $this->redirect(['controller' => 'Posts', 'action' => 'index']);
    }

    public function dislike($id)
    {
        $post = $this->Posts->get($id);
        $user = $this->Authentication->getIdentity();
        if ($post != null) {
            $url =  "http://" . $_SERVER['HTTP_HOST'] . '/microblog4api/api/dislike/' . $id . '.json';
            $session = $this->request->getSession();
            $token = $session->read('token');
            $http = new Client([
                'headers' => ['Authorization' => $token],
            ]);

            $response = $http->delete($url);
            $dislikePost = json_decode($response->getStringBody(), true);

            if ($dislikePost['status']['code'] == 200) {
                $this->Flash->success(__('Disliked'));
            } elseif ($dislikePost['status']['code'] == 404) {
                $this->Flash->error(__('Like first the post.'));
            } else {
                $this->Flash->error(__('Like first the post.'));
            }
        }

        $userAuthorized = $this->Users->get($user['id']);
        $this->Authorization->authorize($userAuthorized);

        return $this->redirect(['controller' => 'Posts', 'action' => 'index']);
    }
}
