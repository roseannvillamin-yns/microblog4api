<?php

declare(strict_types=1);

namespace App\Controller;

use Cake\I18n\FrozenTime;
use App\Controller\AppController;
use Cake\Http\Client;

class FollowersController extends AppController
{
    public function follow($id)
    {
        $following = $this->Users->get($id);
        $this->Authorization->authorize($following);
        $url =  "http://" . $_SERVER['HTTP_HOST'] . '/microblog4api/api/follow/' . $id . '.json';
        $session = $this->request->getSession();
        $token = $session->read('token');
        $http = new Client([
            'headers' => ['Authorization' => $token],
        ]);

        $response = $http->post($url);
        $follow = json_decode($response->getStringBody(), true);
        if ($follow['status']['code'] == 200) {
            $this->Flash->success(__('Followed'));
        } elseif ($follow['status']['code'] == 201) {
            $this->Flash->success(__('Followed'));
        } elseif ($follow['status']['code'] == 404) {
            $this->Flash->error(__('User not found.'));
        } else {
            $this->Flash->error(__('You are already follower of this user.'));
        }

        return $this->redirect(['controller' => 'Posts', 'action' => 'index']);
    }

    public function unfollow($id)
    {
        $following = $this->Users->get($id);
        $this->Authorization->authorize($following);
        $user = $this->Authentication->getIdentity();

        if ($following != null) {
            $url =  "http://" . $_SERVER['HTTP_HOST'] . '/microblog4api/api/unfollow/' . $id . '.json';
            $session = $this->request->getSession();
            $token = $session->read('token');
            $http = new Client([
                'headers' => ['Authorization' => $token],
            ]);

            $response = $http->post($url);
            $unfollow = json_decode($response->getStringBody(), true);

            if ($unfollow['status']['code'] == 200) {
                $this->Flash->success(__('Unfollowed'));
            } elseif ($unfollow['status']['code'] == 404) {
                $this->Flash->error(__('Faild to unfollow.'));
            } else {
                $this->Flash->error(__('You are not following this user.'));
            }
        } else {
            $this->Flash->error(__('Data does not exist.'));
        }
        return $this->redirect(['controller' => 'Posts', 'action' => 'index']);
    }

    public function viewFollowers()
    {
        $user = $this->Authentication->getIdentity();
        $url =  "http://" . $_SERVER['HTTP_HOST'] . '/microblog4api/api/viewFollowers.json';
        $session = $this->request->getSession();
        $token = $session->read('token');
        $http = new Client([
            'headers' => ['Authorization' => $token],
        ]);
        $response = $http->get($url);
        $followers = json_decode($response->getStringBody(), true);
        if ($followers['status']['code'] == 200) {
            $followers['user_followers'] = $this->Paginator->paginate(
                $this->Followers,
                $followers['page_settings']
            );
            $userFollowers = $followers['user_followers'];
            $this->set(compact('userFollowers'));
        }

        $userAuthorized = $this->Users->get($user['id']);
        $this->Authorization->authorize($userAuthorized);

         $this->set('title', 'Microblog 4 - Followers');

        return $this->userInfo();
    }

    public function viewFollowing()
    {
        $user = $this->Authentication->getIdentity();
        $url =  "http://" . $_SERVER['HTTP_HOST'] . '/microblog4api/api/viewFollowing.json';
        $session = $this->request->getSession();
        $token = $session->read('token');
        $http = new Client([
            'headers' => ['Authorization' => $token],
        ]);
        $response = $http->get($url);
        $following = json_decode($response->getStringBody(), true);
        if ($following['status']['code'] == 200) {
            $following['user_following'] = $this->Paginator->paginate(
                $this->Followers,
                $following['page_settings']
            );
            $userFollowing = $following['user_following'];
            $this->set(compact('userFollowing'));
        }

        $userAuthorized = $this->Users->get($user['id']);
        $this->Authorization->authorize($userAuthorized);

        $this->set('title', 'Microblog 4 - Following');

        return $this->userInfo();
    }
}
