<?php
namespace App\Error;

use Cake\Error\ExceptionRenderer;

class AppExceptionRenderer extends ExceptionRenderer
{
    public function notFound($error)
    {
    	$status = [
    		'type' => 'Failed',
    		'code' => 404,
    		'message' => 'Not Found',
    	];
    	$response = $this->controller->getResponse();

        return $response->withType('application/json')
            ->withStatus($status['code'])
            ->withStringBody(json_encode($status));
    }

    public function methodNotAllowed($error)
    {
    	$status = [
    		'type' => 'Failed',
    		'code' => 405,
    		'message' => 'Method Not Allowed'
    	];
    	$response = $this->controller->getResponse();

        return $response->withType('application/json')
            ->withStatus($status['code'])
            ->withStringBody(json_encode($status));
    }

    public function unauthorized($error)
    {
    	$header = $this->request->getHeaderLine('Authorization');
    	if (empty($header)) {
    		$status['message'] = 'Authorization key is null';
    	} else {
    		$status['message'] = 'Unauthorized';
    	}
    	$status = [
    		'type' => 'Failed',
    		'code' => 401,
    		'message' => $status['message']
    	];
    	$response = $this->controller->getResponse();

        return $response->withType('application/json')
            ->withStatus($status['code'])
            ->withStringBody(json_encode($status));
    }

    public function internalServerError($error)
    {
    	$status = [
    		'type' => 'Failed',
    		'code' => 500,
    		'message' => 'Internal Server Error',
    	];
    	$response = $this->controller->getResponse();

        return $response->withType('application/json')
            ->withStatus($status['code'])
            ->withStringBody(json_encode($status));
    }

    public function forbidden($error)
    {
    	$status = [
    		'type' => 'Failed',
    		'code' => 403,
    		'message' => 'Forbidden',
    	];
    	$response = $this->controller->getResponse();

        return $response->withType('application/json')
            ->withStatus($status['code'])
            ->withStringBody(json_encode($status));
    }
}