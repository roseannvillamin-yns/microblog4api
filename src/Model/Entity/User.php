<?php
declare(strict_types=1);

namespace App\Model\Entity;

use Authentication\PasswordHasher\DefaultPasswordHasher;
use Cake\ORM\Entity;

/**
 * User Entity
 *
 * @property int $id
 * @property string $full_name
 * @property string $username
 * @property string $email
 * @property string $password
 * @property string|null $profile_pic
 * @property \Cake\I18n\FrozenTime $created
 * @property \Cake\I18n\FrozenTime|null $modified
 * @property string $email_token
 * @property bool $verified
 *
 * @property \App\Model\Entity\Comment[] $comments
 * @property \App\Model\Entity\Follower[] $followers
 * @property \App\Model\Entity\PostLike[] $post_likes
 * @property \App\Model\Entity\Post[] $posts
 * @property \App\Model\Entity\Retweet[] $retweets
 */
class User extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'full_name' => true,
        'username' => true,
        'email' => true,
        'password' => true,
        'profile_pic' => true,
        'created' => true,
        'modified' => true,
        'email_token' => true,
        'verified' => true,
        'comments' => true,
        'followers' => true,
        'post_likes' => true,
        'posts' => true,
        'retweets' => true,
    ];

    /**
     * Fields that are excluded from JSON versions of the entity.
     *
     * @var array
     */
    protected $_hidden = [
        'password',
    ];

    protected function _setPassword(string $password) : ?string
    {
        if (strlen($password) > 0) {
            return (new DefaultPasswordHasher())->hash($password);
        } else {
            return $this->password;
        }
    }
}
