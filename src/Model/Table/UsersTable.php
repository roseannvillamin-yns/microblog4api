<?php
declare(strict_types=1);

namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

class UsersTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);

        $this->setTable('users');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->hasMany('Comments', [
            'foreignKey' => 'user_id',
        ]);
        $this->hasMany('FollowersA', [
            'foreignKey' => 'user_id',
            'className' => 'Followers'
        ]);
        $this->hasMany('FollowersB', [
            'foreignKey' => 'follower_user_id',
            'className' => 'Followers'
        ]);
        $this->hasMany('PostLikes', [
            'foreignKey' => 'user_id',
        ]);
        $this->hasMany('Posts', [
            'foreignKey' => 'user_id',
        ]);
        $this->hasMany('Retweets', [
            'foreignKey' => 'user_id',
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator
            ->requirePresence('full_name')
            ->notEmptyString('full_name')
            ->add('full_name', [
                'alphabet' => [
                    'rule' => ['custom', '/^[a-zA-Z.\s]*$/'],
                    'message' => 'Only letters, white spaces and dot are allowed.'
                ],
                'whiteSpace' => [
                    'rule' => 'notBlank',
                    'message' => 'White spaces only are not allowed.'
                ],
                'length' => [
                    'rule' => ['minLength', 3],
                    'message' => 'Minimum length of 3 characters.'
                ]
            ]);

        $validator
            ->requirePresence('username')
            ->notEmptyString('username')
            ->add('username', [
                'alphabet' => [
                    'rule' => ['custom', '/^[a-zA-Z0-9_]*$/'],
                    'message' => 'Letters, numbers and underscore only.'
                ],
                'length' => [
                    'rule' => ['lengthBetween', 5, 20],
                    'message' => 'Between 5 to 20 characters only.'
                ],
                'unique' => [
                    'rule' => 'validateUnique',
                    'provider' => 'table',
                    'message' => 'This username is already in use.'
                ]
            ]);

        $validator
            ->email('email')
            ->requirePresence('email')
            ->notEmptyString('email')
            ->add('email', [
                'unique' => [
                    'rule' => 'validateUnique',
                    'provider' => 'table',
                    'message' => 'This email is already in use.'
                ]
            ]);

        $validator
            ->requirePresence('password')
            ->notEmptyString('password')
            ->add('password', [
                'length' => [
                    'rule' => ['minLength', 8],
                    'message' => 'Password should have atleast 8 characters.'
                ]
            ]);

        $validator
            ->allowEmptyFile('profile_image')
            ->add('profile_image', [
                'extension' => [
                    'rule' => ['extension', ['jpeg', 'jpg', 'png', 'gif']],
                    'message' => 'Image not valid.'
                ]
            ]);

        $validator
            ->allowEmptyString('new_password')
            ->add('new_password', [
                'length' => [
                    'rule' => ['minLength', 8],
                    'message' => 'Password should have atleast 8 characters.'
                ]
            ]);

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules): RulesChecker
    {
        $rules->add($rules->isUnique(['username']), ['errorField' => 'username']);
        $rules->add($rules->isUnique(['email']), ['errorField' => 'email']);

        return $rules;
    }
}
