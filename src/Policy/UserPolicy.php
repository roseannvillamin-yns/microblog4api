<?php

declare(strict_types=1);

namespace App\Policy;

use App\Model\Entity\user;
use Authorization\IdentityInterface;
use Authorization\Policy\Result;

/**
 * user policy
 */
class UserPolicy
{
    /**
     * Check if $user can add user
     *
     * @param \Authorization\IdentityInterface $user The user.
     * @param \App\Model\Entity\user $resource
     * @return bool
     */
    public function canAdd(IdentityInterface $user, user $resource)
    {
    }

    public function canEdit(IdentityInterface $user, user $resource)
    {
        return $this->isAuthor($user, $resource);
    }

    // Api Controller
    public function canEditUser(IdentityInterface $user, user $resource)
    {
        return $this->isAuthor($user, $resource);
    }

    /**
     * Check if $user can view user
     *
     * @param \Authorization\IdentityInterface $user The user.
     * @param \App\Model\Entity\user $resource
     * @return bool
     */
    public function canView(IdentityInterface $user, user $resource)
    {
        if ($resource['deleted'] == 0) {
            return true;
        } else {
            return false;
        }
    }

    // Api Controller
    public function canViewUser(IdentityInterface $user, user $resource)
    {
        if ($resource['deleted'] == 0) {
            return true;
        } else {
            return false;
        }
    }

    public function canViewUsers(IdentityInterface $user, user $resource)
    {
        return true;
    }

    public function canFollow(IdentityInterface $user, user $resource)
    {
        return true;
    }

    public function canUnfollow(IdentityInterface $user, user $resource)
    {
        return true;
    }

    public function canViewFollowing(IdentityInterface $user, user $resource)
    {
        return true;
    }

    public function canViewFollowers(IdentityInterface $user, user $resource)
    {
        return true;
    }

    public function canLike(IdentityInterface $user, user $resource)
    {
        return true;
    }

    public function canDislike(IdentityInterface $user, user $resource)
    {
        return true;
    }

    protected function isAuthor(IdentityInterface $user, user $resource)
    {
        return $resource->id === $user->getIdentifier();
    }
}
