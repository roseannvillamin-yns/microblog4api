<?php

declare(strict_types=1);

namespace App\Policy;

use App\Model\Entity\Post;
use Authorization\IdentityInterface;

/**
 * Post policy
 */
class PostPolicy
{
    /**
     * Check if $user can add Post
     *
     * @param \Authorization\IdentityInterface $user The user.
     * @param \App\Model\Entity\Post $post
     * @return bool
     */
    public function canAdd(IdentityInterface $user, Post $post)
    {
        return true;
    }

    // Api Controller
    public function canAddPost(IdentityInterface $user, Post $post)
    {
        return true;
    }


    /**
     * Check if $user can edit Post
     *
     * @param \Authorization\IdentityInterface $user The user.
     * @param \App\Model\Entity\Post $post
     * @return bool
     */
    public function canEdit(IdentityInterface $user, Post $post)
    {
        return $this->isAuthor($user, $post);
    }

    // Api Controller
    public function canEditPost(IdentityInterface $user, Post $post)
    {
        return $this->isAuthor($user, $post);
    }

    /**
     * Check if $user can delete Post
     *
     * @param \Authorization\IdentityInterface $user The user.
     * @param \App\Model\Entity\Post $post
     * @return bool
     */
    public function canDelete(IdentityInterface $user, Post $post)
    {
        return $this->isAuthor($user, $post);
    }

    // Api Controller
    public function canDeletePost(IdentityInterface $user, Post $post)
    {
        return $this->isAuthor($user, $post);
    }

    /**
     * Check if $user can view Post
     *
     * @param \Authorization\IdentityInterface $user The user.
     * @param \App\Model\Entity\Post $post
     * @return bool
     */
    public function canView(IdentityInterface $user, Post $post)
    {
        if ($post['deleted'] == 0) {
            return true;
        } else {
            return false;
        }
    }

    public function canIndex(IdentityInterface $user, Post $post)
    {
        return true;
    }

    public function canUserInfo(IdentityInterface $user, Post $post)
    {
        return true;
    }

    public function canSearch(IdentityInterface $user, Post $post)
    {
        return true;
    }

    public function canRetweet(IdentityInterface $user, Post $post)
    {
        return true;
    }

    protected function isAuthor(IdentityInterface $user, Post $post)
    {
        return $post->user_id === $user->getIdentifier();
    }
}
