<?php

declare(strict_types=1);

namespace App\Policy;

use App\Model\Entity\Comment;
use Authorization\IdentityInterface;

/**
 * Comment policy
 */
class CommentPolicy
{
    /**
     * Check if $user can add Comment
     *
     * @param \Authorization\IdentityInterface $user The user.
     * @param \App\Model\Entity\Comment $comment
     * @return bool
     */
    public function canAdd(IdentityInterface $user, Comment $comment)
    {
        return true;
    }

    // Api Controller
    public function canAddComment(IdentityInterface $user, Comment $comment)
    {
        return true;
    }

    /**
     * Check if $user can edit Comment
     *
     * @param \Authorization\IdentityInterface $user The user.
     * @param \App\Model\Entity\Comment $comment
     * @return bool
     */
    public function canEdit(IdentityInterface $user, Comment $comment)
    {
        return $this->isAuthor($user, $comment);
    }

    // Api Controller
    public function canEditComment(IdentityInterface $user, Comment $comment)
    {
        return $this->isAuthor($user, $comment);
    }

    /**
     * Check if $user can delete Comment
     *
     * @param \Authorization\IdentityInterface $user The user.
     * @param \App\Model\Entity\Comment $comment
     * @return bool
     */
    public function canDelete(IdentityInterface $user, Comment $comment)
    {
        return $this->isAuthor($user, $comment);
    }

    public function canDeleteComment(IdentityInterface $user, Comment $comment)
    {
        return $this->isAuthor($user, $comment);
    }

    /**
     * Check if $user can view Comment
     *
     * @param \Authorization\IdentityInterface $user The user.
     * @param \App\Model\Entity\Comment $comment
     * @return bool
     */
    public function canView(IdentityInterface $user, Comment $comment)
    {
    }

    protected function isAuthor(IdentityInterface $user, Comment $comment)
    {
        return $comment->user_id === $user->getIdentifier();
    }
}
