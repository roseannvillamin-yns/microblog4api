<?php

declare(strict_types=1);

namespace App\Policy;

use App\Model\Entity\Retweet;
use Authorization\IdentityInterface;

/**
 * Retweet policy
 */
class RetweetPolicy
{
    public function canDelete(IdentityInterface $user, Retweet $retweet)
    {
        return $this->isAuthor($user, $retweet);
    }

    // Api Controller
    public function canDeleteRetweet(IdentityInterface $user, Retweet $retweet)
    {
        return $this->isAuthor($user, $retweet);
    }

    protected function isAuthor(IdentityInterface $user, Retweet $retweet)
    {
        return $retweet->user_id === $user->getIdentifier();
    }
}
