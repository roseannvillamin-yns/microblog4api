<html>
<head>
<title></title>
</head>
<body>
<section id='home' class='container'>
    <div class='col-md-12'>
        <div class='row'>
            <?= $this->element('profile') ?>
            <div class='col-md-6'>
                <div class='card2'>
                    <h3 align='center'>Blog Post</h3>
                    <hr>
                    <?php if ($post['user']['profile_pic'] != null) : ?>
                        <?= $this->Html->image(
                            'profiles/' . h($post['user']['profile_pic']),
                            [
                                'class' => 'img-circle'
                            ]
                        ) ?>
                    <?php else : ?>
                        <?= $this->Html->image(
                            'profiles/user.png',
                            [
                                'class' => 'img-circle'
                            ]
                        ) ?>
                    <?php endif; ?>
                    <?= h($post['user']['username']) ?>
                    <h4><?= h($post['post']) ?></h4>
                    <p>
                    <?php if ($post['image'] != null) : ?>
                        <?php if (file_exists(WWW_ROOT . 'img/postImages/' . $post['image'])) : ?>
                            <?= $this->Html->image(
                                'postImages/' . $post['image'],
                                ['class' => 'img-post']
                            ) ?>
                        <?php else : ?>
                            <?= $this->Html->image(
                                'postImages/unavailable-image.jpg',
                                ['class' => 'img-post']
                            ) ?>
                        <?php endif; ?>
                    <?php endif; ?>
                    </p>
                    <small>
                        <?= h($post['created']) ?>
                        <br>
                        <?= h($commentCount) . ' Comments' ?>
                    </small>
                    <br>
                    <?php foreach ($postComments as $postComment) : ?>
                        <?php if ($postComment['user']['profile_pic'] != null) : ?>
                            <?= $this->Html->image(
                                'profiles/' . h($postComment['user']['profile_pic']),
                                [
                                    'class' => 'img-circle-comment'
                                ]
                            ) ?>
                        <?php else : ?>
                            <?= $this->Html->image(
                                'profiles/user.png',
                                [
                                    'class' => 'img-circle-comment'
                                ]
                            ) ?>
                        <?php endif; ?>
                        <?= $this->Html->link(
                            h($postComment['user']['username']),
                            [
                                'controller' => 'Users',
                                'action' => 'view',
                                $postComment['user']['username']
                            ],
                            ['style' => 'text-decoration: none']
                        ) ?>
                        <?= nl2br(h($postComment['comment'])) ?>
                        <?php if ($this->Identity->get('id') == $postComment['user_id']) : ?>
                            <?= $this->Html->image(
                                'edit.png',
                                [
                                    'height' => '20px',
                                    'width' => '20px',
                                    'url' => [
                                        'controller' => 'Comments',
                                        'action' => 'edit',
                                        $postComment['id']
                                    ]
                                ]
                            ) ?>
                            <?= $this->Html->link(
                                $this->Html->image(
                                    'delete.png',
                                    [
                                    'height' => '20px',
                                    'width' => '20px'
                                    ]
                                ),
                                [
                                'controller' => 'Comments',
                                'action' => 'delete',
                                $postComment['id']
                                ],
                                [
                                'escape' => false,
                                'confirm' => 'Are you sure you want to delete?'
                                ]
                            ) ?>
                        <?php endif; ?>
                        <br><br>
                    <?php endforeach; ?>
                    
                    <?= $this->Form->create(
                        $comment,
                        [
                            'url' => [
                                'controller' => 'Comments',
                                'action' => 'add',
                                $post['id']
                            ]
                        ]
                    ) ?>
                    <?= $this->Form->control(
                        'comment',
                        [
                            'class' => 'form-control'
                        ]
                    ) ?>
                    <br>
                    <?= $this->Form->button(
                        'Add Comment',
                        ['class' => 'btn btn-primary']
                    ) ?>
                    <?= $this->Form->end() ?>

                    <ul class="pagination" style="text-align: center; display: inline;">
                        <?= $this->Paginator->prev('« Previous') ?>
                        <?= $this->Paginator->next('Next »') ?>
                    </ul>
                    <center><?= 'Page ' . $this->Paginator->counter() ?></center>
                </div>
            </div>
            <?= $this->element('follow') ?>
        </div>
    </div>
</section>

</body>
</html>

