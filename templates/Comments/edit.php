<html>
<head>
<title></title>
</head>
<body>
<section id='home' class='container'>
    <div class='col-md-12'>
        <div class='row'>
            <?= $this->element('profile') ?>
            <div class='col-md-6'>
                <div class='card2'>
                    <h3 align='center'>Blog Post</h3>
                    <hr>
                    
                    <?= $this->Form->create($comment) ?>
                    <?= $this->Form->control(
                        'comment',
                        [
                            'class' => 'form-control'
                        ]
                    ) ?>
                    <br>
                    <?= $this->Form->button(
                        'Update Comment',
                        ['class' => 'btn btn-primary']
                    )
                    ?>
                    <?= $this->Form->end() ?>
                </div>
            </div>
            <?= $this->element('follow') ?>
        </div>
    </div>
</section>
</body>
</html>
