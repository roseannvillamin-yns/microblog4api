<html>
<head>
<title></title>
</head>
<body>
<section id='home' class='container'>
    <div class='col-md-12'>
        <div class='row'>
            <?= $this->element('profile') ?>
            <div class='col-md-6'>
                <div class='card2'>
                    <h3 align='center'>All Users</h3>
                    <hr>
                    <?php foreach ($users as $user) : ?>
                        <div class='card22'>
                            <?php if ($user['profile_pic'] != null) : ?>
                                <?= $this->Html->image(
                                    'profiles/' . h($user['profile_pic']),
                                    [
                                        'class' => 'img-circle'
                                    ]
                                ) ?>
                            <?php else : ?>
                                <?= $this->Html->image(
                                    'profiles/user.png',
                                    [
                                        'class' => 'img-circle'
                                    ]
                                ) ?>
                            <?php endif; ?>
                            <?= h($user['full_name']) ?>
                            <br>
                            <?= $this->Html->link(
                                h($user['username']),
                                [
                                    'controller' => 'Users',
                                    'action' => 'view',
                                    $user['username']
                                ],
                                ['style' => 'text-decoration: none']
                            ) ?>
                            <?php if ($this->Identity->get('id') != $user['id']) : ?>
                                <?= $this->Html->link(
                                    'Follow',
                                    [
                                    'controller' => 'Followers',
                                    'action' => 'follow',
                                    $user['id']
                                    ],
                                    ['style' => 'text-decoration: none']
                                ) ?>
                                <?= $this->Html->link(
                                    'Unfollow',
                                    [
                                    'controller' => 'Followers',
                                    'action' => 'unfollow',
                                    $user['id']
                                    ],
                                    ['style' => 'text-decoration: none']
                                ) ?>
                            <?php endif;?>
                        </div>
                    <?php endforeach; ?>
                    <ul class="pagination" style="text-align: center; display: inline;">
                        <?= $this->Paginator->prev('« Previous') ?>
                        <?= $this->Paginator->next('Next »') ?>
                    </ul>
                    <center><?= 'Page ' . $this->Paginator->counter() ?></center>
                </div>
            </div>
            <?= $this->element('follow') ?>
        </div>
    </div>
</section>

</body>
</html>

