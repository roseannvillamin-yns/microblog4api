<html>
<head>
<title></title>
<?= $this->Html->css('style.css') ?>
<?= $this->Html->script('script.js') ?>
</head>
<body class='text-center'>
<section id='login' class='container'>
    <div class='row'>
        <div class='col-md-6'>
            <div class='card'>
                <h1 class='h3 mb-3 font-weight-normal'>Create Account</h1>
                <?= $this->Html->image(
                    'user.png',
                    ['class' => 'avatar']
                ) ?>
                <br>
                <?= $this->Flash->render() ?>
                <?= $this->Form->create(
                    $user,
                    ['class' => 'form-signin']
                ) ?>
                <?= $this->Form->control(
                    'full_name',
                    [
                        'class' => 'form-control',
                        'placeholder' => 'Full name'
                    ]
                ) ?>
                <?= $this->Form->control(
                    'username',
                    [
                        'class' => 'form-control',
                        'placeholder' => 'Username'
                    ]
                ) ?>
                <?= $this->Form->control(
                    'email',
                    [
                        'class' => 'form-control',
                        'placeholder' => 'Email'
                    ]
                ) ?>
                <?= $this->Form->control(
                    'password',
                    [
                        'class' => 'form-control',
                        'id' => 'password',
                        'placeholder' => 'Password'
                    ]
                ) ?>
                <?= $this->Form->control(
                    'Show Password',
                    [
                        'type' => 'checkbox',
                        'label' => [
                            'class' => 'show-password'
                        ],
                        'onclick' => 'myFunction()'
                    ]
                ) ?>
                <br>
                <?= $this->Form->button(
                    'SIGN UP',
                    ['class' => 'btn btn-lg btn-primary btn-block']
                ) ?>
                <?= $this->Form->end() ?>
            </div>
        </div>
    </div>
</section>
</body>
</html>

