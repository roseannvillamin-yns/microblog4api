<html>
<head>
<title></title>
<?= $this->Html->css('style.css') ?>
<?= $this->Html->script('script.js') ?>
</head>
<body class='text-center'>
<section id='login' class='container'>
<div class='row'>
    <div class='col-md-6'>
        <div class='card'>
        <h1 class="h3 mb-3 font-weight-normal">Change Password</h1>
        <?= $this->Html->image('user.png', ['class' => 'avatar']) ?>
        <br>
        <?= $this->Form->create($user) ?>
        <?= $this->Form->control(
            'new_password',
            ['class' => 'form-control',
                'placeholder' => 'New Password',
                'type' => 'password',
                'id' => 'password',
                'required'
            ]
        ) ?>
        <?= $this->Form->control(
            'Show Password',
            [
                'type' => 'checkbox',
                'onclick' => 'myFunction()'
            ]
        ) ?>
        <br>
        <?= $this->Form->button(
            'Save Password',
            ['class' => 'btn btn-primary btn-sm']
        ) ?>
        <?= $this->Form->end() ?>
        </div>
    </div>
</div>
</section>
</body>
</html>