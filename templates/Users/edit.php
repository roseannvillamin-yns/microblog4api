<html>
<head>
<title></title>
</head>
<body>
<section id='home' class='container'>
    <div class='col-md-12'>
        <div class='row'>
            <?= $this->element('profile') ?>
            <div class='col-md-6'>
                <div class='card2'>
                    <h3 align='center'>User's Account</h3>
                    <hr>
                    <?= $this->Flash->render() ?>
                    <?= $this->Form->create(
                        $user,
                        [
                            'type' => 'file',
                            'class' => 'form-signin'
                        ]
                    ) ?>
                    <?= $this->Form->control(
                        'full_name',
                        [
                            'class' => 'form-control',
                            'placeholder' => 'Full name'
                        ]
                    ) ?>
                    <?= $this->Form->control(
                        'username',
                        [
                            'class' => 'form-control',
                            'placeholder' => 'Username'
                        ]
                    ) ?>
                    <?= $this->Form->control(
                        'email',
                        [
                            'class' => 'form-control',
                            'placeholder' => 'Email'
                        ]
                    ) ?>
                    <?= $this->Form->control(
                        'new_password',
                        [
                            'type' => 'password',
                            'id' => 'password',
                            'class' => 'form-control',
                            'placeholder' => 'New Password'
                        ]
                    ) ?>
                    <?= $this->Form->control(
                        'Show Password',
                        [
                            'type' => 'checkbox',
                            'label' => [
                                'class' => 'show-password'
                            ],
                            'onclick' => 'myFunction()'
                        ]
                    ) ?>
                    <?= $this->Form->control(
                        'profile_image',
                        [
                            'type' => 'file',
                            'class' => 'form-control'
                        ]
                    ) ?>
                    <br>
                    <?= $this->Form->button(
                        'UPDATE',
                        ['class' => 'btn btn-primary']
                    ) ?>
                    <?= $this->Form->end() ?>
                </div>
            </div>
            <?= $this->element('follow') ?>
        </div>
    </div>
</section>

</body>
</html>

