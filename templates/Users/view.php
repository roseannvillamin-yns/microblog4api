<html>
<head>
<title></title>
</head>
<body>
<section id='home' class='container'>
    <div class='col-md-12'>
        <div class='row'>
            <?= $this->element('profile') ?>
            <div class='col-md-6'>
                <div class='card2'>
                    <h3 align='center'>USER'S PAGE</h3>
                    <hr>
                    <div class='text' align='center'>
                        <div class='chip'>
                            <?php if ($userView['profile_pic'] != null) : ?>
                                <?= $this->Html->image(
                                    'profiles/' . h($userView['profile_pic']),
                                    ['class' => 'profile']
                                ) ?>
                            <?php else : ?>
                                <?= $this->Html->image(
                                    'profiles/user.png',
                                    ['class' => 'profile']
                                ) ?>
                            <?php endif; ?>
                            <h2><?= h($userView['full_name']) ?></h2>
                            <p><?= h($userView['username']) ?></p>
                        </div>
                    </div>

                    <?php foreach ($posts as $post) : ?>
                        <?php if ($post['user']['profile_pic'] != null) : ?>
                            <?= $this->Html->image(
                                'profiles/' . h($post['user']['profile_pic']),
                                ['class' => 'img-circle']
                            ) ?>
                        <?php else : ?>
                            <?= $this->Html->image(
                                'profiles/user.png',
                                ['class' => 'img-circle']
                            ) ?>
                        <?php endif; ?>
                        <?= $this->Html->link(
                            h($post['user']['username']),
                            [
                                'controller' => 'Users',
                                'action' => 'view',
                                $post['user']['username']
                            ],
                            ['style' => 'text-decoration: none']
                        ) ?>
                        <br>
                        <?= nl2br(h($post['post'])) ?>
                        <br>
                        <?php if ($post['image'] != null) : ?>
                            <?php if (file_exists(WWW_ROOT . 'img/postImages/' . $post['image'])) : ?>
                                <?= $this->Html->image(
                                    'postImages/' . h($post['image']),
                                    ['class' => 'img-post']
                                ) ?>
                            <?php else : ?>
                                <?= $this->Html->image(
                                    'postImages/unavailable-image.jpg',
                                    ['class' => 'img-post']
                                ) ?>
                            <?php endif; ?>
                        <?php endif; ?>
                        <br>
                        <small><?= h($post['created']) ?></small>
                        <br>
                        <center>
                            <?= $this->Html->link(
                                'Comments',
                                [
                                    'controller' => 'Comments',
                                    'action' => 'add',
                                    $post['id']
                                ],
                                ['class' => 'btn btn-outline-secondary']
                            ) ?>
                            <?= ' ' . $this->Html->link(
                                'Retweet',
                                [
                                    'controller' => 'Retweets',
                                    'action' => 'retweet',
                                    $post['id']
                                ],
                                ['class' => 'btn btn-outline-info']
                            ) ?>
                            <?php foreach ($likes as $like) : ?>
                                <?php if ($like['post_id'] == $post['id'] && $like['user_id'] == $this->Identity->get('id')) : ?>
                                    <?= ' ' . $this->Html->image(
                                        'like.png',
                                        [
                                            'height' => '20px',
                                            'width' => '20px'
                                        ]
                                    ) ?>
                                <?php endif; ?>
                            <?php endforeach; ?>
                            <?= ' ' . $this->Html->link(
                                'Like',
                                [
                                    'controller' => 'PostLikes',
                                    'action' => 'like',
                                    $post['id']
                                ],
                                ['class' => 'btn btn-outline-primary']
                            ) ?>
                            <?= ' ' . $this->Html->link(
                                'Dislike',
                                [
                                    'controller' => 'PostLikes',
                                    'action' => 'dislike',
                                    $post['id']
                                ],
                                ['class' => 'btn btn-outline-dark']
                            ) ?>
                            <?php if ($this->Identity->get('id') == $post['user_id']) : ?>
                                <?= ' ' . $this->Html->image(
                                    'edit.png',
                                    [
                                        'height' => '20px',
                                        'width' => '20px',
                                        'style' => 'margin-left: 170px',
                                        'url' => [
                                            'controller' => 'Posts',
                                            'action' => 'edit',
                                            $post['id']
                                        ]
                                    ]
                                ) ?>
                                <?= ' ' . $this->Html->link(
                                    $this->Html->image(
                                        'delete.png',
                                        [
                                        'height' => '20px',
                                        'width' => '20px'
                                        ]
                                    ),
                                    [
                                    'controller' => 'Posts',
                                    'action' => 'delete',
                                    $post['id']
                                    ],
                                    [
                                    'escape' => false,
                                    'confirm' => 'Are you sure you want to delete?'
                                    ]
                                ) ?>
                            <?php endif; ?>
                        </center>
                        <hr><br>
                    <?php endforeach; ?>
                    <ul class="pagination" style="text-align: center; display: inline;">
                        <?= $this->Paginator->prev('« Previous') ?>
                        <?= $this->Paginator->next('Next »') ?>
                    </ul>
                    <center><?= 'Page ' . $this->Paginator->counter() ?></center>
                </div>
            </div>
            <?= $this->element('follow') ?>
        </div>
    </div>
</section>

</body>
</html>

