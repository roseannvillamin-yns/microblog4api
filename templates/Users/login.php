<html>
<head>
<title></title>
<?= $this->Html->css('style.css') ?>
<?= $this->Html->script('script.js') ?>
</head>
<body class='text-center'>
<section id='login' class='container'>
    <div class='row'>
        <div class='col-md-6'>
            <div class='card'>
                <h1 class='h3 mb-3 font-weight-normal'>Login Page</h1>
                <?= $this->Html->image(
                    'user.png',
                    ['class' => 'avatar']
                ) ?>
                <br>
                <?= $this->Flash->render() ?>
                <?= $this->Form->create(
                    $user,
                    ['class' => 'form-signin']
                ) ?>
                <?= $this->Form->control(
                    'username',
                    [
                        'class' => 'form-control',
                        'placeholder' => 'Username',
                        'div' => [
                            'class' => 'form-label-group'
                        ]
                    ]
                ) ?>
                <?= $this->Form->control(
                    'password',
                    [
                        'class' => 'form-control',
                        'placeholder' => 'Password',
                        'div' => [
                            'class' => 'form-label-group'
                        ]
                    ]
                ) ?>
                <?= $this->Form->control(
                    'Show Password',
                    [
                        'type' => 'checkbox',
                        'onclick' => 'myFunction()'
                    ]
                ) ?>
                <br>
                <?= $this->Form->button(
                    'LOGIN',
                    ['class' => 'btn btn-lg btn-primary btn-block']
                ) ?>
                <?= $this->Form->end() ?>
                <br>
                <?= $this->Html->link(
                    'Forgot Password',
                    [
                        'controller' => 'users',
                        'action' => 'forgotPassword'
                    ],
                    ['style' => 'text-decoration: none']
                ) ?>
            </div>
        </div>
    </div>
</section>
</body>
</html>