<html>
<head>
<title></title>
</head>
<body>
<section id='home' class='container'>
    <div class='col-md-12'>
        <div class='row'>
            <?= $this->element('profile') ?>
            <div class='col-md-6'>
                <div class='card2'>
                    <h3 align='center'>Add Post</h3>
                    <hr>
                    <?= $this->Form->create(
                        $post,
                        [
                            'url' => [
                                'controller' => 'Posts',
                                'action' => 'add'
                            ],
                            'type' => 'file',
                            'class' => 'form-signin'
                        ]
                    ) ?>
                    <?= $this->Form->control(
                        'post',
                        [
                            'class' => 'form-control'
                        ]
                    ) ?>
                    <?= $this->Form->control(
                        'image_file',
                        [
                            'type' => 'file',
                            'class' => 'form-control'
                        ]
                    ) ?>
                    <br>
                    <?= $this->Form->button(
                        'ADD POST',
                        ['class' => 'btn btn-primary']
                    ) ?>
                    <?= $this->Form->end() ?>
                </div>
            </div>
            <?= $this->element('follow') ?>
        </div>
    </div>
</section>

</body>
</html>

