<html>
<head>
<title></title>
</head>
<body>
<section id='home' class='container'>
    <div class='col-md-12'>
        <div class='row'>
            <?= $this->element('profile') ?>
            <div class='col-md-6'>
                <div class='card2'>
                    <h3 align='center'>Edit Post</h3>
                    <hr>
                    <?= $this->Form->create(
                        $post,
                        [
                            'type' => 'file',
                            'class' => 'form-signin'
                        ]
                    ) ?>
                    <?= $this->Form->control(
                        'post',
                        [
                            'class' => 'form-control'
                        ]
                    ) ?>
                    <br>
                    <?php if ($post['image'] != null) : ?>
                        <?php if (file_exists(WWW_ROOT . 'img/postImages/' . $post['image'])) : ?>
                            <?= $this->Html->image(
                                'postImages/' . $post['image'],
                                ['class' => 'img-post']
                            ) ?>
                        <?php else : ?>
                            <?= $this->Html->image(
                                'postImages/unavailable-image.jpg',
                                ['class' => 'img-post']
                            ) ?>
                        <?php endif; ?>
                    <?php endif; ?>
                    <?= $this->Form->control(
                        'image_file',
                        [
                            'type' => 'file',
                            'class' => 'form-control'
                        ]
                    ) ?>
                    <br>
                    <?= $this->Form->button(
                        'UPDATE',
                        ['class' => 'btn btn-primary']
                    ) ?>
                    <?= $this->Form->end() ?>
                </div>
            </div>
            <?= $this->element('follow') ?>
        </div>
    </div>
</section>

</body>
</html>


