<html>
<head>
<title></title>
</head>
<body>
<section id='home' class='container'>
    <div class='col-md-12'>
        <div class='row'>
            <?= $this->element('profile') ?>
            <div class='col-md-6'>
                <div class='card2'>
                    <h3 align='center'>Search Results</h3>
                    <hr>
                    <?php foreach ($results as $result) : ?>
                        <?php if ($result['user']['profile_pic'] != null) : ?>
                            <?= $this->Html->image(
                                'profiles/' . h($result['user']['profile_pic']),
                                [
                                    'class' => 'img-circle'
                                ]
                            ) ?>
                        <?php else : ?>
                            <?= $this->Html->image(
                                'profiles/user.png',
                                [
                                    'class' => 'img-circle'
                                ]
                            ) ?>
                        <?php endif; ?>
                        <?= $this->Html->link(
                            h($result['user']['full_name']),
                            [
                                'controller' => 'Users',
                                'action' => 'view',
                                $result['user']['username']
                            ],
                            ['style' => 'text-decoration: none']
                        ) ?>
                        <br>
                        <?= h($result['user']['username']) ?>
                        <br>
                        <?= nl2br(h($result['post'])) ?>
                        <br><br>
                        <center>
                            <?= $this->Html->link(
                                'Comments',
                                [
                                    'controller' => 'Comments',
                                    'action' => 'add',
                                    $result['id']
                                ],
                                ['class' => 'btn btn-outline-secondary']
                            ) ?>
                            <?= ' ' . $this->Html->link(
                                'Retweet',
                                [
                                    'controller' => 'Retweets',
                                    'action' => 'retweet',
                                    $result['id']
                                ],
                                ['class' => 'btn btn-outline-info']
                            ) ?>
                            <?php foreach ($likes as $like) : ?>
                                <?php if ($like['post_id'] == $result['id'] && $like['user_id'] == $this->Identity->get('id')) : ?>
                                    <?= ' ' . $this->Html->image(
                                        'like.png',
                                        [
                                            'height' => '20px',
                                            'width' => '20px'
                                        ]
                                    ) ?>
                                <?php endif; ?>
                            <?php endforeach; ?>
                            <?= ' ' . $this->Html->link(
                                'Like',
                                [
                                    'controller' => 'PostLikes',
                                    'action' => 'like',
                                    $result['id']
                                ],
                                ['class' => 'btn btn-outline-primary']
                            ) ?>
                            <?= ' ' . $this->Html->link(
                                'Dislike',
                                [
                                    'controller' => 'PostLikes',
                                    'action' => 'dislike',
                                    $result['id']
                                ],
                                ['class' => 'btn btn-outline-dark']
                            ) ?>
                            <?php if ($this->Identity->get('id') == $result['user_id']) : ?>
                                <?= ' ' . $this->Html->image(
                                    'edit.png',
                                    [
                                        'height' => '20px',
                                        'width' => '20px',
                                        'style' => 'margin-left: 170px',
                                        'url' => [
                                            'controller' => 'Posts',
                                            'action' => 'edit',
                                            $result['id']
                                        ]
                                    ]
                                ) ?>
                                <?= $this->Html->link(
                                    $this->Html->image(
                                        'delete.png',
                                        [
                                        'height' => '20px',
                                        'width' => '20px'
                                        ]
                                    ),
                                    [
                                    'controller' => 'Posts',
                                    'action' => 'delete',
                                    $result['id']
                                    ],
                                    [
                                    'escape' => false,
                                    'confirm' => 'Are you sure you want to delete?'
                                    ]
                                ) ?>
                            <?php endif; ?>
                        </center>
                        <hr>
                    <?php endforeach; ?>
                    <ul class="pagination" style="text-align: center; display: inline;">
                        <?= $this->Paginator->prev('« Previous') ?>
                        <?= $this->Paginator->next('Next »') ?>
                    </ul>
                    <center><?= 'Page ' . $this->Paginator->counter() ?></center>
                </div>
            </div>
            <?= $this->element('follow') ?>
        </div>
    </div>
</section>

</body>
</html>

