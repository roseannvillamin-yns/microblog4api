<!DOCTYPE html>
<html>
<head>
	<title>Error Page</title>
</head>
<body>
	<?= $this->element('navbar_error') ?>
	<br>
	<div class="jumbotron jumbotron-fluid">
		<div class="container">
			<div class="alert alert-danger" role="alert">
				<h1 class="display-4">Controller Page Does Not Exist</h1>
			</div>
		</div>
	</div>
	
</body>
</html>