<?= $this->Html->script('bootstrap.min4.js') ?>
<?= $this->Html->css('style.css') ?>
<nav class="navbar navbar-dark fixed-top bg-dark">
    <div class="container">
        <a class="navbar-brand" href="#">MICROBLOG 4</a>
        <div class="btn-group">
            <?php $session = $this->request->getSession(); ?>
            <?php if ($session->read('Auth.id') != null) : ?>
                <?= $this->Html->link(
                    'HOME',
                    [
                    'controller' => 'Posts',
                    'action' => 'index',
                    ],
                    [
                    'class' => 'nav-link',
                    'style' => 'color: white'
                    ]
                ) ?>
            <?php else : ?>
                <?= $this->Html->link(
                    'LOGIN',
                    [
                    'controller' => 'Users',
                    'action' => 'login'
                    ],
                    [
                    'class' => 'btn btn-light',
                    'rule' => 'button'
                    ]
                ) ?>
            <?php endif; ?>
        </div>
    </div>
</nav>
