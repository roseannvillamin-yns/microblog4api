<nav class='navbar navbar-dark fixed-top bg-dark'>
	<div class='container'>
		<a class='navbar-brand' href='#'>MICROBLOG 4</a>
		<?= $this->Html->link(
			'SIGN UP',
		    [
		        'controller' => 'Users',
		        'action' => 'add',
		    ],
		    [
		        'class' => 'btn btn-light',
		        'rule' => 'button'
		    ]
		) ?>
	</div>
</nav>