<?= $this->Html->css('home_style.css') ?>
<div class='col-md-6'>
    <div class='card2'>
        <h3 align='center'>BLOG POSTS</h3>
        <hr>
        <?php foreach ($posts as $post) : ?>
            <?php if ($post['user']['profile_pic'] != null) : ?>
                <?= $this->Html->image(
                    'profiles/' . $post['user']['profile_pic'],
                    [
                        'class' => 'img-circle'
                    ]
                ) ?>
            <?php else : ?>
                <?= $this->Html->image(
                    'profiles/user.png',
                    [
                        'class' => 'img-circle'
                    ]
                ) ?>
            <?php endif; ?>
            <?= $this->Html->link(
                $post['user']['username'],
                [
                    'controller' => 'Users',
                    'action' => 'view',
                    $post['user']['username']
                ],
                ['style' => 'text-decoration: none']
            ) ?>
            <br>
            <?= nl2br(h($post['post'])) ?>
                <br>
            <?php if ($post['image'] != null) : ?>
                <?php if (file_exists(WWW_ROOT . 'img/postImages/' . $post['image'])) : ?>
                    <?= $this->Html->image(
                        'postImages/' . $post['image'],
                        ['class' => 'img-post']
                    ) ?>
                <?php else : ?>
                    <?= $this->Html->image(
                        'postImages/unavailable-image.jpg',
                        ['class' => 'img-post']
                    ) ?>
                <?php endif; ?>
            <?php endif; ?>
            <br>
            <small><?= h($post['created']) ?></small>
            <br>
            <center>
                <?= $this->Html->link(
                    'Comments',
                    [
                        'controller' => 'Comments',
                        'action' => 'add',
                        $post['id']
                    ],
                    ['class' => 'btn btn-outline-secondary']
                ) ?>
                <?php foreach ($retweets as $retweet) : ?>
                    <?php
                    if ($retweet['post_id'] == $post['id'] && $retweet['user_id'] == $this->Identity->get('id')) : ?>
                        <?= ' ' . $this->Html->image(
                            'retweet.png',
                            [
                                'height' => '20px',
                                'width' => '20px'
                            ]
                        ) ?>
                    <?php endif; ?>
                    <?php break ?>
                <?php endforeach; ?>
                <?= ' ' . $this->Html->link(
                    'Retweet',
                    [
                        'controller' => 'Retweets',
                        'action' => 'retweet',
                        $post['id']
                    ],
                    ['class' => 'btn btn-outline-info']
                ) ?>
                <?php foreach ($likes as $like) : ?>
                    <?php if ($like['post_id'] == $post['id'] && $like['user_id'] == $this->Identity->get('id')) : ?>
                        <?= ' ' . $this->Html->image(
                            'like.png',
                            [
                                'height' => '20px',
                                'width' => '20px'
                            ]
                        ) ?>
                    <?php endif; ?>
                <?php endforeach; ?>
                <?= ' ' . $this->Html->link(
                    'Like',
                    [
                        'controller' => 'PostLikes',
                        'action' => 'like',
                        $post['id']
                    ],
                    ['class' => 'btn btn-outline-primary']
                ) ?>
                <?= ' ' . $this->Html->link(
                    'Dislike',
                    [
                        'controller' => 'postLikes',
                        'action' => 'dislike',
                        $post['id']
                    ],
                    ['class' => 'btn btn-outline-dark']
                ) ?>
            </center>
            <hr><br>
        <?php endforeach; ?>
        <ul class="pagination" style="text-align: center; display: inline;">
            <?= $this->Paginator->prev('« Previous') ?>
            <?= $this->Paginator->next('Next »') ?>
        </ul>
        <center><?= 'Page ' . $this->Paginator->counter() ?></center>
    </div>
</div>
