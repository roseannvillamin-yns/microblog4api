<?= $this->Html->css('home_style.css') ?>
<div class='col-md-3'>
    <div class='card3'>
        <?= $this->Form->create(
            null,
            [
                'type' => 'get',
                'url' => [
                    'controller' => 'Posts',
                    'action' => 'search'
                ],
                ['class' => 'd-flex']
            ]
        ) ?>
        <?= $this->Form->control(
            'keyword',
            [
                'value' => $this->request->getQuery('keyword'),
                'class' => 'form-control me-2',
                'placeholder' => 'Search',
                'div' => false,
                'label' => false,
                'style' => 'width:235px;'
            ]
        ) ?>
        <?= $this->Form->button(
            'Search',
            [
                'class' => 'btn btn-primary',
                'div' => false,
                'style' => 'margin-top: -65px; margin-left: 240px '
            ]
        ) ?>
        <?= $this->Form->end() ?>
        <center style='background-color: #8FDDE7'>
            <?= $this->Html->link(
                'List of Users',
                [
                    'controller' => 'Users',
                    'action' => 'viewUsers'
                ],
                ['style' => 'text-decoration: none']
            ) ?>
        </center>
        <hr>
        <center style='background-color: #8FDDE7'>
            Following
        </center>
        <div class='following-scroll'>
        <?php foreach ($following as $follow) : ?>
            <br>
            <?php if ($follow['users_following']['profile_pic'] != null) : ?>
                <?= $this->Html->image(
                    'profiles/' . h($follow['users_following']['profile_pic']),
                    [
                        'class' => 'img-circle'
                    ]
                ) ?>
            <?php else : ?>
                <?= $this->Html->image(
                    'profiles/user.png',
                    [
                        'class' => 'img-circle'
                    ]
                ) ?>
            <?php endif; ?>
            <?= h($follow['users_following']['username']) ?>
            <?= $this->Html->link(
                'Unfollow',
                [
                    'controller' => 'followers',
                    'action' => 'unfollow',
                    $follow['users_following']['id']
                ],
                ['style' => 'text-decoration: none']
            ) ?>
        <?php endforeach; ?>
        </div>
        <center>
        <?= $this->Html->link(
            'View All',
            [
                'controller' => 'Followers',
                'action' => 'viewFollowing'
            ],
            ['class' => 'btn btn-primary btn-sm']
        ) ?>
        </center>
        <hr>
        <center style='background-color: #8FDDE7'>
            Followers
        </center>
        <div class='follower-scroll'>
        <?php foreach ($followers as $follower) : ?>
        <br>
            <?php if ($follower['users_follower']['profile_pic'] != null) : ?>
                <?= $this->Html->image(
                    'profiles/' . h($follower['users_follower']['profile_pic']),
                    [
                        'class' => 'img-circle'
                    ]
                ) ?>
            <?php else : ?>
                <?= $this->Html->image(
                    'profiles/user.png',
                    [
                        'class' => 'img-circle'
                    ]
                ) ?>
            <?php endif; ?>
            <?= h($follower['users_follower']['username']) ?>
            <?= $this->Html->link(
                'Follow',
                [
                    'controller' => 'Followers',
                    'action' => 'follow',
                    $follower['users_follower']['id']
                ],
                ['style' => 'text-decoration: none']
            ) ?>
        <?php endforeach; ?>
        </div>
        <center>
        <?= $this->Html->link(
            'View All',
            [
                'controller' => 'Followers',
                'action' => 'viewFollowers'
            ],
            ['class' => 'btn btn-primary btn-sm']
        ) ?>
        </center>
    </div>
</div>
