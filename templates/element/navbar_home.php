<?= $this->Html->script('bootstrap.min4.js') ?>
<?= $this->Html->css('style.css') ?>
<nav class="navbar navbar-dark fixed-top bg-dark">
    <div class="container">
        <a class="navbar-brand" href="#">MICROBLOG 4</a>
        <div class="btn-group">
            <?= $this->Html->link(
                'HOME',
                [
                    'controller' => 'Posts',
                    'action' => 'index',
                ],
                [
                    'class' => 'nav-link',
                    'style' => 'color: white'
                ]
            ) ?>
            <div class="dropdown">
                <button class="btn btn-dark dropdown-toggle" type="button" data-toggle="dropdown">
                    <?php if ($this->Identity->get('id') != null) : ?>
                        <?= $this->Identity->get('username') ?>
                    <?php endif; ?>
                    <span class="caret"></span>
                </button>
                <ul class="dropdown-menu">
                    <li>
                        <?= $this->Html->link(
                            'Logout',
                            [
                                'controller' => 'Users',
                                'action' => 'logout'
                            ],
                            [
                                'class' => 'dropdown-item'
                            ]
                        ) ?>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</nav>
