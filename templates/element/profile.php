<?= $this->Html->css('home_style.css') ?>
<div class='col-md-3'>
    <div class='card'>
        <div class='chip'>
            <?php if ($this->Identity->get('profile_pic') != null) : ?>
                <?= $this->Html->image(
                    'profiles/' . h($this->Identity->get('profile_pic')),
                    ['class' => 'profile']
                ) ?>
            <?php else : ?>
                <?= $this->Html->image(
                    'profiles/user.png',
                    ['class' => 'profile']
                ) ?>
            <?php endif; ?>
        </div>
        <div class='text' align='center'>
            <h1 class='h3 mb-3 font-weight-normal' align='center'>
                <?php if ($this->Identity->get('id') != null) : ?>
                    <?= $this->Html->link(
                        h($this->Identity->get('full_name')),
                        [
                            'controller' => 'Users',
                            'action' => 'view',
                            $this->Identity->get('username')
                        ],
                        ['style' => 'text-decoration: none']
                    ) ?>
                <?php endif; ?>
            </h1>
            <h4><?= h($this->Identity->get('username')) ?></h4>
            <small><?= h($this->Identity->get('email')) ?></small>
            <br><br>
            <div class='container'>
                <div class='row'>
                    <div class='col-sm'>
                        <h5>Following</h5>
                        <?= h($followingCount) ?>
                    </div>
                    <div class='col-sm'>
                        <h5>Followers</h5>
                        <?= h($followerCount) ?>
                    </div>
                    <div class='col-sm'>
                        <h5>Likes</h5>
                        <?= h($likeCount) ?> 
                    </div>
                </div>
            </div>
            
            <br>
            <?= $this->Html->link(
                'Add Post',
                [
                    'controller' => 'Posts',
                    'action' => 'add'
                ],
                ['class' => 'btn btn-primary btn-sm']
            ) ?>
            <?php if ($this->Identity->get('id') != null) : ?>
                <?= ' ' . $this->Html->link(
                    'Account Settings',
                    [
                        'controller' => 'Users',
                        'action' => 'edit',
                        $this->Identity->get('id')
                    ],
                    ['class' => 'btn btn-secondary btn-sm']
                ) ?>
            <?php endif; ?>
        </div>
        <br><br>
        <div class='text' align='center'>
            <h5 style='background-color: #8FDDE7'>
                Retweeted Posts
            </h5>
        </div>
        <div class='retweet-scroll'>
            <?php foreach ($retweets as $retweet) : ?>
                <small>Retweeted by <?= h($retweet['user']['username']) ?></small>
                <br>
                <?php if ($retweet['user']['profile_pic'] != null) : ?>
                    <?= $this->Html->image(
                        'profiles/' . h($retweet['user']['profile_pic']),
                        [
                            'class' => 'img-circle-retweet'
                        ]
                    ) ?>
                <?php else : ?>
                    <?= $this->Html->image(
                        'profiles/user.png',
                        [
                            'class' => 'img-circle-retweet'
                        ]
                    ) ?>
                <?php endif; ?>
                <br>
                <?= h($retweet['post']['post']) ?>
                <br>
                <center>
                <?php if ($retweet['post']['image'] != null) : ?>
                    <?= $this->Html->image(
                        'postImages/' . h($retweet['post']['image']),
                        [
                            'height' => '120px',
                            'width' => '150px'
                        ]
                    ) ?>
                <?php endif; ?>
                </center>
                <br>
                <small>
                    Retweeted date: <?= h($retweet['created']) ?>
                    <?= $this->Html->link(
                        $this->Html->image(
                            'retweet.png',
                            [
                            'height' => '20px',
                            'width' => '20px'
                            ]
                        ),
                        [
                        'controller' => 'Retweets',
                        'action' => 'retweet',
                        $retweet['post_id']
                        ],
                        [
                        'escape' => false,
                        'confirm' => 'Are you sure you want to retweet?'
                        ]
                    ) ?>
                    <?php if ($this->Identity->get('id') == $retweet['user_id']) : ?>
                        <?= $this->Html->link(
                            $this->Html->image(
                                'delete.png',
                                [
                                'height' => '20px',
                                'width' => '20px'
                                ]
                            ),
                            [
                            'controller' => 'Retweets',
                            'action' => 'delete',
                            $retweet['id']
                            ],
                            [
                            'escape' => false,
                            'confirm' => 'Are you sure you want to delete?'
                            ]
                        ) ?>
                    <?php endif; ?>
                </small>
                <hr>
            <?php endforeach; ?>
        </div>
    </div>
</div>
