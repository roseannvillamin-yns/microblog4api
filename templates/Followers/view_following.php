<html>
<head>
<title></title>
</head>
<body>
<section id='home' class='container'>
    <div class='col-md-12'>
        <div class='row'>
            <?= $this->element('profile') ?>
            <div class='col-md-6'>
                <div class='card2'>
                    <h3 align='center'>Following</h3>
                    <hr>
                    
                    <?php foreach ($userFollowing as $userFollow) : ?>
                        <?php if ($userFollow['users_following']['profile_pic'] != null) : ?>
                            <?= $this->Html->image(
                                'profiles/' . h($userFollow['users_following']['profile_pic']),
                                [
                                    'class' => 'img-circle'
                                ]
                            ) ?>
                        <?php else : ?>
                            <?= $this->Html->image(
                                'profiles/user.png',
                                [
                                    'class' => 'img-circle'
                                ]
                            ) ?>
                        <?php endif; ?>
                        <?= h($userFollow['users_following']['full_name']) ?>
                        <br>
                        <?= $this->Html->link(
                            h($userFollow['users_following']['username']),
                            [
                                'controller' => 'Users',
                                'action' => 'view',
                                $userFollow['users_following']['username']
                            ],
                            ['style' => 'text-decoration: none']
                        ) ?>
                        <br><br>
                    <?php endforeach; ?>
                    <ul class="pagination" style="text-align: center; display: inline;">
                        <?= $this->Paginator->prev('« Previous') ?>
                        <?= $this->Paginator->next('Next »') ?>
                    </ul>
                    <center><?= 'Page ' . $this->Paginator->counter() ?></center>
                </div>
            </div>
            <?= $this->element('follow') ?>
        </div>
    </div>
</section>

</body>
</html>


